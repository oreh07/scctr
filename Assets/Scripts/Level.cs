﻿using UniLua;
using UnityEngine;
using DG.Tweening;

public class Level : LuaResponder_moon
{
    public string levelName;

    private Bot bot;
    private int botCounter = 0;
    private BotController bc;

    public float MovementSpeed { get; private set; }

    public void Activate() {
        bc = FindObjectOfType<BotController>();
        type = "level";
        botCounter = 0;

        LoadScript(levelName);
        CallCoroutine("init");
    }

    void Update()
    {
        transform.Translate(Vector3.back * MovementSpeed * Time.deltaTime);
    }

    protected override int LevelCreateObject(string name)
    {
        var obj = Instantiate(Resources.Load("Prefabs\\" + name), transform);
        obj.name = System.IO.Path.GetFileName(name);
        return 1;
    }
    protected override int LevelDestroyObject(string name)
    {
        Destroy(GetGameElement(name).gameObject);
        return 1;
    }
    protected override int LevelMoveElement(string name, string vector, float time)
    {
        Vector3 target = vector.Parse();
        GameElement g = GameElement.GetElement(name);
        if (time == 0)
        {
            g.transform.position = target;
        }
        else
        {
            g.transform.DOMove(target, time);
        }
        return 1;
    }
    protected override int LevelRotateElement(string name, string vector, float time)
    {
        Vector3 target = vector.Parse();
        GameElement g = GameElement.GetElement(name);
        if (time == 0)
        {
            g.transform.rotation = Quaternion.Euler(target);
        }
        else
        {
            g.transform.DORotate(target, time);
        }
        return 1;
    }
    protected override int LevelMoveMap(float speed)
    {
        MovementSpeed = speed;
        return 1;
    }
    protected override int LevelShowElement(string name, float time)
    {
        GetGameElement(name).SetActive(true, time);
        return 1;
    }
    protected override int LevelHideElement(string name, float time)
    {
        GetGameElement(name).SetActive(false, time);
        return 1;
    }
    protected override int LevelShakePositionElement(string name, float time, float strength, int vibrato)
    {
        GameElement.GetElement(name).transform.DOShakePosition(time, strength, vibrato);
        return 1;
    }
    protected override int LevelShakeRotationElement(string name, float time, float strength, int vibrato)
    {
        GameElement.GetElement(name).transform.DOShakeRotation(time, strength, vibrato);
        return 1;
    }
    protected override int LevelSetText(string name, string text)
    {
        UIGameElement textf = (UIGameElement) GameElement.GetElement(name);
        textf.SetText(text);
        return 1;
    }
    protected override int Spawn(string name)
    {
        botCounter++;
        bot = bc.CreateUnit(name);
        return 1;
    }
    protected override int start(ILuaState s)
    {
        if(bot == null)
        {
            Debug.LogWarning("unit not found " + botCounter);
            return 1;
        }
        bot.SetStartPoint(new Vector3((float)s.L_CheckNumber(1), 0, (float)s.L_CheckNumber(2)));
        return 1;
    }
    protected override int setPoint(ILuaState s)
    {
        if (bot == null)
        {
            Debug.LogWarning("unit not found " + botCounter);
            return 1;
        }
        bot.SetPoint(bot.pointCount.ToString(), new Vector3((float)s.L_CheckNumber(1), 0, (float)s.L_CheckNumber(2)));
        return 1;
    }
    protected override int setPointNamed(ILuaState s)
    {
        if (bot == null)
        {
            Debug.LogWarning("unit not found " + botCounter);
            return 1;
        }
        bot.SetPoint(s.L_CheckString(1), new Vector3((float)s.L_CheckNumber(2), 0, (float)s.L_CheckNumber(3)));
        return 1;
    }
    protected override int end(ILuaState s)
    {
        if (bot == null)
        {
            Debug.LogError("unit not found " + botCounter);
            return 1;
        }
        bot.SetPoint("end", new Vector3((float)s.L_CheckNumber(1), 0, (float)s.L_CheckNumber(2)));
        bot.End();
        bot = null;
        return 1;
    }


    private GameElement GetGameElement(string name)
    {
        GameElement obj = GameElement.GetElement(name);
        if (obj == null)
        {
            Debug.LogError("can't find gameElement named [" + name + "]");
            return null;
        }
        return obj;
    }
}
