﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class ItemObjectContainer : MonoBehaviour {

    Rigidbody rb;
    Level lvl;

    public ItemType holdingItem;
    
    [SerializeField]
    float floatingTime = 5f;
    float floatTimer = 0f;

    
	void Start () {
        rb = GetComponent<Rigidbody>();
        lvl = LevelController.Instance.currentLevel;
	}
    private void Update()
    {
        if (floatTimer < floatingTime)
        {
            floatTimer += Time.deltaTime;
        }
        else
        {
            rb.velocity = Vector3.back * 1f * lvl.MovementSpeed;
        }
    }
}
