﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemUI : MonoBehaviour {
    private static ItemType _selected;
    public static ItemType selected {
        get
        {
            return _selected;
        }
        set
        {
            _selected = value;
            UpdateFrame();
        }
    }

    public ItemType type;

    public GameObject selectionFrame;

    static void UpdateFrame()
    {
        ItemUI[] list = FindObjectsOfType<ItemUI>();
        foreach (ItemUI i in list)
        {
            i.selectionFrame.SetActive(i.type == selected);
        }
    }
}
