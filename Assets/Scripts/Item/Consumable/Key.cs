﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : ConsumableItem
{
    public override void Activate()
    {
        base.Activate();
        PlayerController.Instance.keys += 1;
    }
}
