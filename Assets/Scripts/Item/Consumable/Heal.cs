﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heal : ConsumableItem
{
    public override void Activate()
    {
        base.Activate();
        PlayerController.Instance.player.Heal(1f);
    }
}
