﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class ItemObject : MonoBehaviour {
    public ItemType item;

    public void Init()
    {
        ItemStat stat = ItemController.Instance.GetItemStats(item);
        GetComponent<SpriteRenderer>().sprite = stat.sprite;
    }

    private bool CanPick()
    {
        ItemStat stat = ItemController.Instance.GetItemStats(item);
        if (stat.ItemClass == ItemClass.Consumable)
        {
            if (item == ItemType.Heal)
            {
                PlayerUnit p = PlayerController.Instance.player;
                if (p.healthValue == p.healthMax)
                {
                    return false;
                }
            }
        }
        ItemController.Instance.AddItem(item);
        return true;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player") && CanPick())
        {
            Debug.LogWarning("itemObject do nothing!");
            Destroy(gameObject);
        }
    }
}
