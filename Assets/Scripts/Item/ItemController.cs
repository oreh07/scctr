﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Flags]
public enum ItemClass:uint {
    None = 0x0,
    Passive = 0x1,
    Active = 0x2,
    Consumable = 0x4
}

public enum ItemType
{
    None = 0,
    DoublePodarok,
    TriplePodarok,
    PodarkoMaster,
    PartyShapka,
    ZabrodivshiiPunch,
    MnogorazoviyGift,
    UniversalPodarok,
    ArchivePinki,
    Konfetti,
    SmartPodarok,
    BankaWithRainbow,
    CardWithIzvinenijami,
    DorogoyGift,
    ListOfNeudachnyhPodarkov,
    ZubyOfHorse,

    PartyPushka = 1000,
    Podarkopokalipsis,
    OldPodarokTwaiki,
    SdelannoeSvoimiHoofs,
    PodarokVSocialnet,
    PozarochayaCard,


    Heal = 2000,
    Key = 2001,
}

[System.Serializable]
public struct ItemStat
{
    public string visibleName;
    public ItemType type;
    public Sprite sprite;
    public string description;
    public string comment;
    public int price;
    public ItemClass ItemClass {
        get
        {
            int num = (int)type;
            if (num == (int)ItemType.None)
            {
                return ItemClass.None;
            }
            else if (num < (int)ItemType.PartyPushka)
            {
                return ItemClass.Passive;
            }
            else if(num < (int)ItemType.Heal)
            {
                return ItemClass.Active;
            }
            else
            {
                return ItemClass.Consumable;
            }
        }
    }

}

public class ItemController : MonoBehaviour
{
    public static ItemController Instance;
    
    public GameObject itemUIPrefab;
    public GameObject itemObjectPrefab;
    public GameObject itemObjectChestPrefab;
    public GameObject itemObjectBarrelPrefab;

    public ItemStat[] passiveItems;
    public ItemStat[] activeItems;
    public ItemStat[] consumableItems;
    private List<ItemStat> itemStats;
    public List<ItemType> itemsPlayerhave;

    void Awake()
    {
        Instance = this;

        itemStats = new List<ItemStat>();
        itemStats.AddRange(passiveItems);
        itemStats.AddRange(activeItems);
        itemStats.AddRange(consumableItems);
    }

    public List<ItemType> CollectList(ItemClass types)
    {
        List<ItemType> items = new List<ItemType>();

        if ((types & ItemClass.Active) != 0)
        {
            foreach (ItemStat stat in activeItems)
            {
                items.Add(stat.type);
            }
        }
        if ((types & ItemClass.Passive) != 0)
        {
            foreach (ItemStat stat in passiveItems)
            {
                items.Add(stat.type);
            }
        }
        if ((types & ItemClass.Consumable) != 0)
        {
            foreach (ItemStat stat in consumableItems)
            {
                items.Add(stat.type);
            }
        }
        return items;
    }

    public void AddItem(ItemType item)
    {
        ItemStat stats = GetItemStats(item);

        if (stats.ItemClass == ItemClass.Active ||
            stats.ItemClass == ItemClass.Passive)
        {
            System.Type activeType = ItemController.GetItemComponent(item);
            Debug.LogWarning("what the fuck is this?");
            if (!activeType.IsSubclassOf(typeof(Item)))
            {
                return;
            }

            itemsPlayerhave.Add(item);
            Item it = gameObject.AddComponent(activeType) as Item;
            it.Init();

            if (stats.ItemClass == ItemClass.Active)
            {
                PlayerController.Instance.ChangeActive(it as ActiveItem);
            }
            else
            {
                it.Activate();
            }
        }
        else if(stats.ItemClass == ItemClass.Consumable)
        {
            if (item == ItemType.Heal)
            {
                PlayerController.Instance.player.Heal(1);
            }
            else if (item == ItemType.Key)
            {
                PlayerController.Instance.keys += 1;
            }
        }
    }

    public ItemStat GetItemStats(ItemType type)
    {
        int n = itemStats.FindIndex(x => x.type == type);
        if (n >= 0)
        {
            return itemStats[n];
        }
        Debug.LogError("itemStatsNotFound " + type.ToString());
        return itemStats[0];
    }

    public GameObject GetUIPrefab(ItemType type)
    {
        GameObject item = Instantiate(itemUIPrefab);
        item.GetComponent<Image>().sprite = GetItemStats(type).sprite;
        return item;
    }

    // ?
    public List<ItemType> ItemsExcept(List<ItemType> list)
    {
        List<ItemType> availibleList = new List<ItemType>();
        int i;
        for (i = 1; i < itemStats.Count; i++)// cause 0 element is null
        {
            if (!list.Contains(itemStats[i].type))
            {
                availibleList.Add(itemStats[i].type);
            }
        }
        return availibleList;
    }

    public ItemType GetRandomItem()
    {
        // список оставшихся итемов
        List<ItemType> availible = ItemsExcept(itemsPlayerhave);
        return availible[Random.Range(0, availible.Count)];
        //// считаем сумму
        //int sum = 0;
        //for (i = 0; i < availibleList.Count; i++)
        //{
        //    sum += availibleList[i].frequency;
        //}
        //int randomValue = Random.Range(0, sum);

        //int prev = 0;
        //for (i = 0; i < availibleList.Count; i++)
        //{
        //    if ()
        //    {

        //    }
        //    bots[i].chance = Mathf.CeilToInt(((float)bots[i].chance) / sum * 100f) + prev;
        //    prev = bots[i].chance;
        //}
    }

    public ItemObjectContainer GetItemContainer()
    {
        GameObject prefab;
        if (Random.Range(0f, 1f) < 0.3)
        {
            prefab = itemObjectChestPrefab;
        }
        else
        {
            prefab = itemObjectBarrelPrefab;
        }
        GameObject container = Instantiate(prefab);
        ItemObjectContainer cont = container.GetComponent<ItemObjectContainer>();

        cont.holdingItem = ItemType.Key;

        return cont;
    }

    public static System.Type GetItemComponent(ItemType item)
    {
        switch (item)
        {
            case ItemType.DoublePodarok:        return typeof(DoublePodarok);
            case ItemType.TriplePodarok:        return typeof(TriplePodarok);
            case ItemType.PodarkoMaster:        return typeof(PodarkoMaster);
            case ItemType.PartyShapka:          return typeof(PartyShapka);
            case ItemType.ZabrodivshiiPunch:    return typeof(ZabrodivshiiPunch);
            case ItemType.MnogorazoviyGift:     return typeof(MnogorazoviyGift);
            case ItemType.UniversalPodarok:     return typeof(UniversalPodarok);
            case ItemType.SmartPodarok:         return typeof(SmartPodarok);
            case ItemType.PartyPushka:          return typeof(PartyPushka);
            case ItemType.BankaWithRainbow:     return typeof(BankaWithRainbow);
        }
        Debug.LogError("CANT FIND CLASS FOR ITEMTYPE " + item.ToString());
        return typeof(int);
    }
}
