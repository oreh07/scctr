﻿using UnityEngine;

public abstract class Item
{
    protected ItemController ic;
    public ItemType item;

    public virtual void Init() {
        ic = ItemController.Instance;
    }
    public virtual void Activate()
    {
        Debug.LogFormat("<color=red>Activate {0}</color>", item);
    }
    public virtual void Deactivate()
    {
        Debug.LogFormat("<color=red>Deactivate {0}</color>", item);
    }
}
