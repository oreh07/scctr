﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartyShapka : PassiveItem {

    public override void Init()
    {
        base.Init();
        item = ItemType.PartyShapka;
    }

    public override void Activate()
    {
        base.Activate();
        if (pc.player.shooter is BulletShooter)
        {
            BulletShooter bs = pc.player.shooter as BulletShooter;
            bs.bulletSpeedMultiple *= 1.2f;
        }

    }

    public override void Deactivate()
    {
        base.Deactivate();

        if (pc.player.shooter is BulletShooter)
        {
            BulletShooter bs = pc.player.shooter as BulletShooter;
            bs.bulletSpeedMultiple /= 1.2f;
        }
    }
}
