﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MnogorazoviyGift : PassiveItem {

    public override void Init()
    {
        base.Init();
        item = ItemType.DoublePodarok;
    }

    public override void Activate()
    {
        base.Activate();
        if (pc.player.shooter is BulletShooter)
        {
            BulletShooter bs = pc.player.shooter as BulletShooter;
            bs.pierce += 1;
        }
        
    }

    public override void Deactivate()
    {
        base.Deactivate();

        if (pc.player.shooter is BulletShooter)
        {
            BulletShooter bs = pc.player.shooter as BulletShooter;
            bs.pierce -= 1;
        }
    }
}
