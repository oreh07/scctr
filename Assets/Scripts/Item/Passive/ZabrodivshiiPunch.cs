﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZabrodivshiiPunch : PassiveItem {
    public override void Init()
    {
        base.Init();
        item = ItemType.TriplePodarok;
    }

    public override void Activate()
    {
        base.Activate();
        pc.player.shooter.type = BulletType.Snake;
    }

    public override void Deactivate()
    {
        base.Deactivate();
        pc.player.shooter.type = BulletType.Standart;
    }
}
