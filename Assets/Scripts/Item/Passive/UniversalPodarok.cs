﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UniversalPodarok : PassiveItem {

    public override void Init()
    {
        base.Init();
        item = ItemType.UniversalPodarok;
    }

    public override void Activate()
    {
        base.Activate();
        if (pc.player.shooter is BulletShooter)
        {
            BulletShooter bs = pc.player.shooter as BulletShooter;
            bs.type = BulletType.Flower;
            bs.bulletSpeedMultiple *= 1.5f;
            bs.damageMult *= 0.6f;
            bs.reloadTime *= 0.4f;
        }
    }

    public override void Deactivate()
    {
        base.Deactivate();

        if (pc.player.shooter is BulletShooter)
        {
            BulletShooter bs = pc.player.shooter as BulletShooter;
            bs.type = BulletType.Standart;
            bs.bulletSpeedMultiple /= 1.5f;
            bs.damageMult /= 0.6f;
            bs.reloadTime /= 0.4f;
        }
    }
}
