﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmartPodarok : PassiveItem {

    public override void Init()
    {
        base.Init();
        item = ItemType.SmartPodarok;
    }

    public override void Activate()
    {
        base.Activate();
        pc.player.shooter.isHoming = true;
    }

    public override void Deactivate()
    {
        base.Deactivate();
        pc.player.shooter.isHoming = false;
    }
}
