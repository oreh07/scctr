﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriplePodarok : PassiveItem {
    public override void Init()
    {
        base.Init();
        item = ItemType.TriplePodarok;
    }

    public override void Activate()
    {
        base.Activate();
        pc.player.shooter.attacksPerShoot *= 3;
    }

    public override void Deactivate()
    {
        base.Deactivate();
        pc.player.shooter.attacksPerShoot /= 3;
    }
}
