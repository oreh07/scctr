﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BankaWithRainbow : PassiveItem
{
    public override void Init()
    {
        base.Init();
        item = ItemType.BankaWithRainbow;
    }

    public override void Activate()
    {
        base.Activate();

        PlayerUnit unit = PlayerController.Instance.player;

        Shooter currentShooter = unit.GetComponent<Shooter>();
        Destroy(currentShooter);

        LaserShooter laserShooter = unit.gameObject.AddComponent<LaserShooter>();
        currentShooter.Copy(laserShooter);

    }

    public override void Deactivate()
    {
        base.Deactivate();
        Debug.LogError("BankaWithRainbow not realized deactivate");
    }
}
