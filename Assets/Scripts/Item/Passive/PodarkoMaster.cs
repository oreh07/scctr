﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PodarkoMaster : PassiveItem {

    public override void Init()
    {
        base.Init();
        item = ItemType.PodarkoMaster;
    }

    public override void Activate()
    {
        base.Activate();
        pc.player.shooter.reloadTime /= 1.2f;
    }

    public override void Deactivate()
    {
        base.Deactivate();
        pc.player.shooter.reloadTime *= 1.2f;
    }
}
