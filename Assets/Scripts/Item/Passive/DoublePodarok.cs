﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoublePodarok : PassiveItem
{
    public override void Init()
    {
        base.Init();
        item = ItemType.DoublePodarok;
    }

    public override void Activate()
    {
        base.Activate();
        pc.player.shooter.attacksPerShoot *= 2;
    }

    public override void Deactivate()
    {
        base.Deactivate();
        pc.player.shooter.attacksPerShoot /= 2;
    }
}
