﻿using UnityEngine;

public class ActiveItem : Item
{
    public float cooldownTime;
    public bool isSleep = false;
    public float cTimer = 0;

    public override void Activate()
    {
        base.Activate();
        isSleep = true;
        cTimer = cooldownTime;
    }

    public virtual void Reload()
    {
        cTimer = 0;
        isSleep = false;
    }

    public void UpdateState()
    {
        if (isSleep)
        {
            cTimer -= Time.deltaTime;
            if (cTimer <= 0)
            {
                Reload();
            }
        }
    }
}
