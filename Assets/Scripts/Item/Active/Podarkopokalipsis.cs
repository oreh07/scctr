﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Podarkopokalipsis : ActiveItem {

    public override void Init()
    {
        item = ItemType.PartyPushka;
        cooldownTime = 5f;
    }

    public override void Activate()
    {
        base.Activate();

        Transform player = PlayerController.Instance.player.transform;

        GameObject b = Instantiate(Resources.Load<GameObject>("Prefabs/PartyPushkaBullet"));
        b.transform.position = player.position;

        PartyPushkaBullet bl = b.GetComponent<PartyPushkaBullet>();
        bl.pierce = 999;
        bl.movement = player.forward;
        bl.side = ShellSide.Player;
        bl.Turn(true);
    }

    public override void Reload()
    {
        base.Reload();
    }
}
