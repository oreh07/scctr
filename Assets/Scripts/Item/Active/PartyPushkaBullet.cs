﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartyPushkaBullet : Bullet {

    protected override void OnCollideWall()
    {
        Destroy(this.gameObject);
    }

    protected override void OnCollideEnemy(Unit u)
    {
        bc.CreateGib(transform.position, u.transform.position);
        u.Hit(damage);
    }
}
