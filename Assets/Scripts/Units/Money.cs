﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Money : Shell
{
    private Transform playerTransform;
    public float sqrRange = 4f;
    public int moneyValue = 1;
    public Vector3 speed = Vector3.zero;

    public override void Setup()
    {
        base.Setup();
        playerTransform = PlayerController.Instance.player.transform;
    }

    public override void Update()
    {
        speed *= 0.98f;
        speed.z -= 0.1f;

        transform.Translate(speed * Time.deltaTime);


        Vector3 dv = playerTransform.position - transform.position;
        float dist = Vector3.SqrMagnitude(dv);
        if (dist < 3f)
        {
            GiveMoney();
            Turn(false);
        }
        if (dist < sqrRange)
        {
            transform.Translate(dv.normalized * Time.deltaTime * Mathf.Lerp(0f, 20f, 1 - dist/sqrRange));
        }
        if (Mathf.Abs(transform.position.x) > 11 || Mathf.Abs(transform.position.z) > 14)
        {
            Turn(false);
        }
    }

    private void GiveMoney()
    {
        PlayerController.Instance.money += moneyValue;
    }
}
