﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotController : MonoBehaviour {
    public static BotController Instance;
    public Transform botContainer;

    public List<Bot> bots;

    void Start()
    {
        Instance = this;
    }

    public Bot CreateUnit(string unitName)
    {
        GameObject go;
        go = Resources.Load<GameObject>("Prefabs/Bots/" + unitName);//botList.Find(x => x.name.Contains(unitName));
        if (go == null)
        {
            Resources.Load<GameObject>("Prefabs/Bots/Creep");
            Debug.LogError("Cant find bot with name " + unitName + "!");
        }
        go = Instantiate(go);
        go.name = unitName;
        go.transform.SetParent(botContainer);

        Bot b = go.GetComponent<Bot>();
        bots.Add(b);
        return b;
    }

    public void DeleteBot(Bot b)
    {
        bots.Remove(b);
        Destroy(b.gameObject);
    }

    public void DestroyAll()
    {
        int count = bots.Count;
        for (int i = 0; i < count; i++)
        {
            DeleteBot(bots[0]);
        }
    }
}
