﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniLua;
using UnityEngine;
using DG.Tweening;

public class Bot : Unit {

    // look at DG.Tweening.Ease
    public enum EaseType
    {
        easelin = 1,
        easein = 5,
        easeout = 6,
        easeio = 7
    }
    [System.Serializable]
    struct BulletProperties
    {
        public string name;
        public float timeout;
        public BulletType bullet;
        public float direction;
    }

    public EaseType easetype;
    public float movementSpeed;
    public int moneyReward;
    public float shootingTimout;
    [SerializeField]
    BulletProperties[] bulletTypes;

    protected MeshRenderer modelRenderer;
    public Dictionary<string, Vector3> points;
    public int pointCount {
        get { return points.Count; }
    }


    private bool isEndMovement = false;
    protected BotController bc;


	//protected override int botPointsNumber (ILuaState s)
	//{
	//	s.PushNumber(pointCount);
	//	return 1;
	//}

    void Awake()
    {
        gameObject.SetActive(false);
        points = new Dictionary<string, Vector3>();
    }

    protected override void Init()
    {
        base.Init();
        //print("init");
        RestoreDefaultStats();
        bc = FindObjectOfType<BotController>();
        gameObject.SetActive(true);
        LoadScript(name);

        modelRenderer = model.GetComponent<MeshRenderer>();
    }

    public void SetStartPoint(Vector3 v)
    {
        //print("setstartpoint " + v);
        transform.position = v;
    }

    public void SetPoint(string key, Vector3 v)
    {
        //print("setpoint " + v);
        if (points.ContainsKey(key))
        {
            Debug.LogWarning("Bot[" + name + "] rewrite point named [" + key + "] with point: " + v.ToString());
            points.Remove(key);
        }
        points.Add(key, v);
    }

    public void End()
    {
        Init();
    }

    public override void Shoot(float angle = 0, string shooterName = "")
    {
        if (shooter)
        {
            if (shooterName.Length == 0)
            {
                shooter.Shoot(Quaternion.Euler(0, angle, 0) * Vector3.forward, side);
            }
            else
            {
                (shooter as MultipleShooter).Shoot(Quaternion.Euler(0, angle, 0) * Vector3.forward, shooterName, side);
            }
        }
    }

    protected override int getRotationFrom(ILuaState s)
    {
        if (shooter is MultipleShooter)
        {
            Vector3 p = (shooter as MultipleShooter).GetShooter(s.L_CheckString(1)).transform.position;
            s.PushNumber(MathUtils.AngleFromTo(p, pc.player.transform.position));
        }
        else
        {
            s.PushNumber(MathUtils.AngleFromTo(transform.position, pc.player.transform.position));
        }
        return 1;
    }

    protected override int getRotation(ILuaState s)
    {
        s.PushNumber(MathUtils.AngleFromTo(transform.position, pc.player.transform.position));
        return 1;
    }

    protected override int shootLua(ILuaState s)
    {
        Shoot((float)s.L_CheckNumber(1));
        return 1;
    }

    protected override int shootFromLua(ILuaState s)
    {
        if (shooter is MultipleShooter)
        {
            Shoot((float)s.L_CheckNumber(2), s.L_CheckString(1));
        }
        else
        {
            Shoot((float)s.L_CheckNumber(1));
        }
        return 1;
    }

    protected override int moveToEnd(ILuaState s)
    {
        isEndMovement = true;
        if (points.ContainsKey("end"))
        {
            string ease = "ease" + s.L_CheckString(2);
            EaseType easeType = (EaseType)Enum.Parse(typeof(EaseType), ease, true);

            MoveToPoint(points["end"], (float)s.L_CheckNumber(1), easeType);
        }
        else
        {
            Debug.LogError("can't find end of " + name);
        }
        
        return s.YieldK(s.GetTop(), 0, null);
    }
    protected override int moveToSaved(ILuaState s)
    {
        if (points.ContainsKey(s.L_CheckString(1)))
        {
            Vector3 v = points[s.L_CheckString(1)];
            float stepTime = (float)s.L_CheckNumber(2);
            string ease = "ease" + s.L_CheckString(3);
            EaseType easeType = (EaseType)Enum.Parse(typeof(EaseType), ease, true);

            MoveToPoint(v, stepTime, easeType);
        }
        else
        {
            Debug.LogError("Cant find point named " + s.L_CheckString(1));
        }
        return s.YieldK(s.GetTop(), 0, null);
    }

    protected override int moveToPoint(float x, float y)
    {
        //Vector3 v = new Vector3((float)s.L_CheckNumber(1), 0, (float)s.L_CheckNumber(2));
        //float stepTime = (float)s.L_CheckNumber(3);
        //string ease = "ease" + s.L_CheckString(4);
        //EaseType easeType = (EaseType)Enum.Parse(typeof(EaseType), ease, true);

        //MoveToPoint(v, stepTime, easeType);
        //return s.YieldK(s.GetTop(), 0, null);
        return 0;
    }

    protected virtual void MoveToPoint(Vector3 v, float time, EaseType ease)
    {
        DOTween.To(
            () => transform.position,
            x => transform.position = x,
            v,
            time
            ).OnComplete(OnMoveComplete).SetEase((Ease)ease);
    }

    private void OnMoveComplete()
    {
        if (isEndMovement)
        {
            bc.DeleteBot(this);
        }
        LuaResume();
    }

    protected override void Death()
    {
        base.Death();
        bc.DeleteBot(this);
        BulletController.Instance.GetMoney(this.transform.position, this);

        TrySpawnItemObject();
    }

    public override bool Hit(float value)
    {
        base.Hit(value);
        if (!isInvulnerable)
        {
            StartCoroutine(LateHitAnimation());
        }
        return healthValue > 0;
    }

    IEnumerator LateHitAnimation()
    {
        modelRenderer.material.color = Color.red;
        yield return new WaitForSeconds(0.05f);
        modelRenderer.material.color = Color.white;
    }

    private void TrySpawnItemObject()
    {
        return;
        //ItemObjectContainer cont = ItemController.Instance.GetItemContainer();
        //cont.holdingItem = ItemType.Key;
        //cont.transform.SetParent(this.transform.parent, false);

    }
}
