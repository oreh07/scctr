﻿using System.Collections;
using System.Collections.Generic;
using UniLua;
using UnityEngine;

public class Boss : Bot {
    protected override void Init()
    {
        base.Init();
    }

    protected override int initBoss(ILuaState s)
    {
        BossHealthController.Instance.Show();
        return 1;
    }

    public override bool Hit(float value)
    {
        base.Hit(value);
        BossHealthController.Instance.UpdateHealth(healthValue / healthMax);
        return healthValue > 0;
    }
}
