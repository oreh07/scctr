﻿using System.Collections;
using System.Collections.Generic;
using UniLua;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Unit : LuaResponder_moon, IStatRestoreItem
{
    protected PlayerController pc;

    [SerializeField]
    private float _healthValue;
    public virtual float healthValue
    {
        get
        {
            return _healthValue;
        }
        set
        {
            _healthValue = value;
        }
    }
    public int healthMax = 6;

    protected GameObject model;
    protected bool isInvulnerable = false;
    [HideInInspector] public ShellSide side;
    [HideInInspector] public Rigidbody rb;
    [HideInInspector] public Animator animator;
    [SerializeField] bool isFlying;

    private Shooter _shooter;
    public Shooter shooter
    {
        get
        {
            if (_shooter == null)
            {
                Shooter[] shoots = GetComponents<Shooter>();
                if (shoots.Length == 0)
                {
                    _shooter = gameObject.AddComponent<BulletShooter>();
                }
                else
                {
                    _shooter = shoots[shoots.Length - 1];
                }
                _shooter.Init();
            }
            return _shooter;
        }
    }

    private StatRestoreController _stats;
    public StatRestoreController stats
    {
        get
        {
            if (_stats == null)
            {
                _stats = new StatRestoreController();
            }
            return _stats;
        }
    }

    protected virtual void Init()
    {
        pc = PlayerController.Instance;
        rb = GetComponent<Rigidbody>();
        animator = GetComponentInChildren<Animator>();

        if (animator != null)
        {
            animator.SetTrigger("changeMovementType");
            animator.SetBool("isFlying", isFlying);
        }

        side = ShellSide.Enemy;
        type = "unit";

        Transform meshtr = transform.Find("mesh");
        if (meshtr == null)
        {
            Debug.LogError("cant find mesh in " + gameObject.name);
        }
        model = meshtr.gameObject;
    }

    public virtual bool Hit(float value)
    {
        if (isInvulnerable)
        {
            return false;
        }
        if (healthValue <= 0)
        {
            return false;
        }
        healthValue -= value;
        if (healthValue <= 0)
        {
            Death();
            return true;
        }
        return false;
    }

    public virtual void Heal(float value)
    {
        healthValue = Mathf.Clamp(healthValue + value, 0, healthMax);
    }

    public void Push(Vector3 v)
    {
        rb.AddForce(v);
    }

    public virtual void Shoot(float angle = 0, string shooterName = "")
    {
        if (shooter)
        {
            if (shooterName.Length == 0)
            {
                shooter.Shoot(transform.forward, side);
            }
            else
            {
                shooter.Shoot(transform.forward, side);
            }

        }
    }

    public virtual void SetInvisible(bool value)
    {
        model.SetActive(!value);

        Collider[] colls = GetComponents<Collider>();
        foreach (Collider c in colls)
        {
            c.enabled = !value;
        }
    }
    public virtual void SetInvulnerable(bool value)
    {
        isInvulnerable = value;
    }

    protected virtual void Death() { }

    protected override int getPosition(ILuaState s)
    {
        string coord = s.L_CheckString(1);
        s.PushNumber((coord == "x") ? transform.position.x : transform.position.y);
        return 1;
    }

    protected override int getUnitHealth(UniLua.ILuaState s)
    {
        s.PushNumber(healthValue / healthMax);
        return 1;
    }

    protected override int setInvulnerableLua(ILuaState s)
    {
        SetInvulnerable(s.L_CheckInteger(1) >= 1);
        return 1;
    }

    public virtual void SaveDefaultStats()
    {
        Debug.LogFormat("<color=grey>unit: [{0}] saving default stats</color>", name);
    }
    public virtual void RestoreDefaultStats()
    {
        Debug.LogFormat("<color=grey>unit: [{0}] restoring default stats</color>", name);

        healthValue = healthMax;
        isInvulnerable = false;
    }
    public virtual void DestroyStats()
    {
        Debug.LogFormat("<color=grey>unit: [{0}] destroy default stats</color>", name);
    }
}
