﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerUnit : Unit
{
    public Rigidbody Rb { get; private set; }
    public override float healthValue
    {
        get
        {
            return base.healthValue;
        }
        set
        {
            base.healthValue = value;
            HealthController.Instance.UpdateHealth();
        }
    }
    public float speedValue = 8;
    public float rotationValue = 3.5f;

    private float hitShakeTime = 1f;

    void Awake()
    {
        Init();
        Rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (animator != null && animator.gameObject.activeSelf)
        {
            Vector3 normalVelocity = rb.velocity.normalized;
            float speed = rb.velocity.magnitude;
            animator.SetFloat("MoveForward", Vector3.Project(normalVelocity, transform.forward).magnitude * speed);
            animator.SetFloat("MoveRight", Vector3.Project(normalVelocity, transform.right).magnitude * speed);
        }
    }

    protected override void Init()
    {
        base.Init();
        side = ShellSide.Player;

        SaveDefaultStats();
    }

    public void MoveTo(Vector3 v)
    {
        v.y = 0;
        rb.AddForce(v * speedValue * 100f * Time.deltaTime);
        //Debug.DrawLine(transform.position, transform.position + rb.velocity.normalized, Color.blue, 0.1f);
    }

    public void RotateTo(float angle)
    {
        Quaternion rot = transform.rotation;
        transform.rotation = Quaternion.Euler(0f, Mathf.LerpAngle(rot.eulerAngles.y, angle, Time.deltaTime * rotationValue), 0f);
    }

    protected override void Death()
    {
        base.Death();
        StartCoroutine(LateDeath());
    }

    public override bool Hit(float value)
    {
        float saved = healthValue;
        base.Hit(value);
        HealthController.Instance.Hit(healthValue);
        if (saved != healthValue && healthValue > 0) // wound
        {
            Camera.main.DOShakePosition(hitShakeTime, 0.3f);
            StartCoroutine(LateHitInvinsible());
        }
        return healthValue > 0;
    }

    public override void Heal(float value)
    {
        base.Heal(value);
        HealthController.Instance.Heal(value);
    }

    IEnumerator LateHitInvinsible()
    {
        int blinks = 8;
        float blinkTime = 2f;
        float blinkHalf = blinkTime / blinks * 0.5f;

        SetInvulnerable(true);
        SetInvisible(true);
        for (int i = 0; i < blinks; i++)
        {
            model.SetActive(false);
            yield return new WaitForSeconds(blinkHalf);
            model.SetActive(true);
            yield return new WaitForSeconds(blinkHalf);
        }
        SetInvulnerable(false);
        SetInvisible(false);
    }

    IEnumerator LateDeath()
    {
        Time.timeScale = 0.3f;
        PlayerController.Instance.AllowControl = false;

        yield return new WaitForSeconds(1f);
        MenuController.Instance.Lose();
        yield return new WaitForSeconds(0.3f);
        Time.timeScale = 1f;
    }

    public override void SaveDefaultStats()
    {
        base.SaveDefaultStats();
        stats.AddStat("healthMax", healthMax);
        stats.AddStat("speed", speedValue);
        stats.AddStat("rotationSpeed", rotationValue);

        shooter.SaveDefaultStats();
    }

    public override void RestoreDefaultStats()
    {
        base.RestoreDefaultStats();
        speedValue = stats.GetStatFloat("speed");
        rotationValue = stats.GetStatFloat("rotationSpeed");

        if (!(shooter is BulletShooter))
        {
            Destroy(shooter);
        }

        // потому что компонент может быть удалён
        StartCoroutine(LateRestoreShooter());
    }
    IEnumerator LateRestoreShooter()
    {
        yield return new WaitForEndOfFrame();

        shooter.RestoreDefaultStats();
    }
}
