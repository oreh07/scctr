﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathUtils {
    public static float AngleFromTo(Vector3 from, Vector3 to)
    {
        Vector3 dm = (from - to).normalized;
        return -Mathf.Atan2(dm.z, dm.x) * 180f / Mathf.PI - 90f;
    }
}
