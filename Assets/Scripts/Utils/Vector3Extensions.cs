﻿using UnityEngine;

public static class Vector3Extension
{
    public static Vector3 Parse(this string vector)
    {
        string[] s = vector.Split(',');
        if(s.Length != 3)
        {
            Debug.LogError("vector [" + vector + "] is not a vector");
            return Vector3.zero;
        }
        return new Vector3(float.Parse(s[0]), float.Parse(s[1]), float.Parse(s[2]));
    }

    public static Vector3 GetRandom(float value)
    {
        return new Vector3(Random.Range(-value, value), Random.Range(-value, value), Random.Range(-value, value));
    }
}
