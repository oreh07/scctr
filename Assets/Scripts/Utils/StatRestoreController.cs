﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatRestoreController
{
    public Dictionary<string, float> statsFloat;
    public Dictionary<string, string> statsString;

    public StatRestoreController()
    {
        statsFloat = new Dictionary<string, float>();
        statsString = new Dictionary<string, string>();
    }

    public void AddStat(string name, float value)
    {
        if (!statsFloat.ContainsKey(name))
        {
            statsFloat.Add(name, value);
            return;
        }
        statsFloat[name] = value;
    }
    public void AddStat(string name, string value)
    {
        if (!statsString.ContainsKey(name))
        {
            statsString.Add(name, value);
            return;
        }
        statsString[name] = value;
    }

    public float GetStatFloat(string name)
    {
        if (statsFloat.ContainsKey(name))
        {
            return statsFloat[name];
        }
        Debug.LogError("cant find " + name);
        return 0f;
    }
    public string GetStatString(string name)
    {
        if (statsString.ContainsKey(name))
        {
            return statsString[name];
        }
        Debug.LogError("cant find " + name);
        return "";
    }

    public void RemoveStat(string name)
    {
        if (statsFloat.ContainsKey(name))
        {
            statsFloat.Remove(name);
        }
        if (statsString.ContainsKey(name))
        {
            statsString.Remove(name);
        }
    }
}
