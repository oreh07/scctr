﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IStatRestoreItem
{
    StatRestoreController stats { get; }

    void SaveDefaultStats();
    void RestoreDefaultStats();
    void DestroyStats();
}
