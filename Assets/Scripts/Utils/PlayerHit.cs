﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHit : MonoBehaviour
{
    public void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            PlayerUnit pu = PlayerController.Instance.player;
            Vector3 collistionPoint = pu.transform.position + (this.transform.position - pu.transform.position).normalized * 0.5f;
            BulletController.Instance.CreateGib(collistionPoint, pu.transform.position);
            pu.Hit(1f);
        }
    }
}
