﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class line : MonoBehaviour {

    List<Vector3> ps;

    void OnDrawGizmos() {

        ps = new List<Vector3>();
        foreach (Transform t in this.transform)
        {
            ps.Add(t.position);
        }

        //DrawDirectLine();

        DrawByCurves(ps);
        DrawSpline();

	}

    void DrawByCurves(List<Vector3> points)
    {
        List<Vector3> pts = new List<Vector3>();

        pts.Add(points[0]);

        float dl = 0;
        for (var i = 0; i < points.Count - 2; i++)
        {
            float dr = (points[i + 2].z - points[i].z) / 2;
            float a3 = dl + dr + 2 * (points[i].z - points[i + 1].z);
            float a2 = points[i + 1].z - a3 - dl - points[i].z;

            for (float t = 0; t <= 1; t += .25f)
            {
                float y = a3;
                y = y * t + a2;
                y = y * t + dl;
                y = y * t + points[i].z;
                pts.Add(new Vector3(points[i].x + t * (points[i + 1].x - points[i].x), 0, y));
            }
            dl = dr;
        }
    }

    float[] dx;
    float[] dy;

    float[] B0;
    float[] B1;
    float[] B2;
    float[] B3;

    void FindCPoints()
    {
        int n = ps.Count - 2;
        int n1 = n + 1;

        dx = new float[n];
        dy = new float[n];

        dx[0] = ps[n].x - ps[0].x;
        dy[0] = ps[n].z - ps[0].z;

        dx[n - 1] = -(ps[n1].x - ps[n - 1].x);
        dy[n - 1] = -(ps[n1].z - ps[n - 1].z);

        float[] Ax = new float[n1];
        float[] Ay = new float[n1];
        float[] Bi = new float[n1];

        Bi[1] = -.25f;
        Ax[1] = (ps[2].x - ps[0].x - dx[0]) / 4;
        Ay[1] = (ps[2].z - ps[0].z - dy[0]) / 4;

        for (var i = 2; i < n - 1; i++)
        {
            Bi[i] = -1 / (4 + Bi[i - 1]);
            Ax[i] = -(ps[i + 1].x - ps[i - 1].x - Ax[i - 1]) * Bi[i];
            Ay[i] = -(ps[i + 1].z - ps[i - 1].z - Ay[i - 1]) * Bi[i];
        }

        for (var i = n - 2; i > 0; i--)
        {
            dx[i] = Ax[i] + dx[i + 1] * Bi[i];
            dy[i] = Ay[i] + dy[i + 1] * Bi[i];
        }
    }

    void GenKoef()
    {
        B0 = new float[26];
        B1 = new float[26];
        B2 = new float[26];
        B3 = new float[26];

        float t = 0;
        for (int i = 0; i < 26; i++)
        {
            float t1 = 1 - t;
            float t12 = t1 * t1;
            float t2 = t * t;

            B0[i] = t1 * t12;
            B1[i] = 3 * t * t12;
            B2[i] = 3 * t2 * t1;
            B3[i] = t * t2;

            t += .04f;
        }
    }

    void DrawSpline()
    {
        GenKoef();

        int n = ps.Count - 2;

        FindCPoints();


        List<Vector3> pts = new List<Vector3>();
        if (n > 1)
        {
            pts.Add(new Vector3(ps[0].x, 0, ps[0].z));

            for (var i = 1; i < n; i++)
            {
                pts.Add(new Vector3(ps[i - 1].x + dx[i - 1], 0, ps[i - 1].z - dy[i - 1]));
                pts.Add(new Vector3(ps[i].x - dx[i], 0, ps[i].z + dy[i]));
                pts.Add(new Vector3(ps[i].x, 0, ps[i].z));
            }

            for (int i = 0; i < pts.Count - 1; i++)
            {
                Debug.DrawLine(pts[i], pts[i + 1], Color.blue);
            }
        }

        pts = new List<Vector3>();
        pts.Add(new Vector3(ps[0].x, 0, ps[0].z));

        float X;
        float Y;
        for (var i = 0; i < n - 1; i++)
        {
            for (var k = 0; k < 26; k++)
            {
                X = (ps[i].x * B0[k] + (ps[i].x + dx[i]) * B1[k] + (ps[i + 1].x - dx[i + 1]) * B2[k] + ps[i + 1].x * B3[k]);
                Y = (ps[i].z * B0[k] + (ps[i].z + dy[i]) * B1[k] + (ps[i + 1].z - dy[i + 1]) * B2[k] + ps[i + 1].z * B3[k]);
                pts.Add(new Vector3(X, 0, Y));
            }
        }

        for (int i = 0; i < pts.Count - 1; i++)
        {
            Debug.DrawLine(pts[i], pts[i + 1], Color.red);
        }
    }

    void DrawDirectLine()
    {

        for (int i = 0; i < ps.Count - 1; i++)
        {
            Debug.DrawLine(ps[i], ps[i + 1], Color.white);
        }
    }
}
