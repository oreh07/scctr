﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShellPool<T> : ObjectPool<T> where T : Shell {

    protected T bulletComponent;

    public override void Init(Transform parent, GameObject _prefab, int defaultCount = 20)
    {
        base.Init(parent, _prefab);
        bulletComponent = prefab.GetComponent<T>();
        container.name = bulletComponent.type.ToString() + "Container";
    }

    public override T GetBullet()
    {
        int count = poolList.Count;
        for (int i = 0; i < poolList.Count; i++)
        {
            if (poolList[i] == null)
            {
                poolList.RemoveAt(i);
                count--;
                i--;
                Debug.LogWarning("BULLET LOST! " + bulletComponent.type.ToString());
            }
            if (!poolList[i].activeInHierarchy)
            {
                return bulletComponent.Copy(poolList[i].GetComponent<T>()) as T;
            }
        }
        AddBullets();
        T result = bulletComponent.Copy(poolList[poolList.Count - 1].GetComponent<T>()) as T;
        return result;
    }

    protected override void SetupNewBullet(T bullet)
    {
        bullet.Turn(false);
    }

    public override void DestroyAllBullets()
    {
        for (int i = 0; i < poolList.Count; i++)
        {
            if (poolList[i].activeInHierarchy)
            {
                poolList[i].GetComponent<T>().Turn(false);
            }
        }
    }
}
