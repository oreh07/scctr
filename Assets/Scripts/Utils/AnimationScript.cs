﻿using UnityEngine;

public class AnimationScript : MonoBehaviour
{
    public bool loop;
    public float frameSeconds = 1;
    public string location; //The file location of the sprites within the resources folder
    private SpriteRenderer spr;
    private Sprite[] sprites;
    private int frame = 0;
    private float deltaTime = 0;
    
    void Start()
    {
        spr = GetComponent<SpriteRenderer>();
        sprites = Resources.LoadAll<Sprite>(location);

        frame = sprites.Length - 1;
    }

    public void Play()
    {
        gameObject.SetActive(true);
        frame = 0;
    }
    
    void Update()
    {
        
        //Keep track of the time that has passed
        deltaTime += Time.deltaTime;

        /*Loop to allow for multiple sprite frame 
         jumps in a single update call if needed
         Useful if frameSeconds is very small*/
        while (deltaTime >= frameSeconds)
        {
            deltaTime -= frameSeconds;
            frame++;

            if (loop)
            {
                frame %= sprites.Length;
            }
            else if (frame == sprites.Length) //Max limit
            {
                frame = sprites.Length - 1;
                gameObject.SetActive(false);
            }
        }
        //Animate sprite with selected frame
        spr.sprite = sprites[frame];
    }
}