﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool<T> where T : MonoBehaviour {
    protected BulletController bc;
    protected GameObject prefab;
    protected List<GameObject> poolList;
    protected Transform container;

    public int Count
    {
        get
        {
            return poolList.Count;
        }
    }

    public virtual void Init(Transform parent, GameObject _prefab, int defaultCount = 20)
    {
        if (_prefab == null)
        {
            Debug.LogError("Cant find prefab for " + typeof(T).ToString());
            return;
        }
        prefab = _prefab;

        poolList = new List<GameObject>();

        container = new GameObject().transform;
        container.SetParent(parent);

        AddBullets(defaultCount);
        bc = BulletController.Instance;
    }

    public void ChangeContainer(Transform con) {
        int count = container.childCount;
        for (int i = 0; i < count; i++)
        {
            container.GetChild(0).SetParent(con);
        }
        container = con;
    }
    
    public virtual T GetBullet()
    {
        for (int i = 0; i < poolList.Count; i++)
        {
            if (!poolList[i].activeInHierarchy)
            {
                return poolList[i].GetComponent<T>();
            }
        }
        AddBullets();
        return poolList[poolList.Count - 1].GetComponent<T>();
    }

    public virtual void AddBullets(int value = 5)
    {
        for (int i = 0; i < value; i++)
        {
            GameObject g = Object.Instantiate(prefab);
            g.transform.SetParent(container);
            SetupNewBullet(g.GetComponent<T>());
            poolList.Add(g);
        }
    }
    protected virtual void SetupNewBullet(T bullet)
    {
        bullet.gameObject.SetActive(false);
    }

    public virtual void DestroyAllBullets()
    {
        for (int i = 0; i < poolList.Count; i++)
        {
            poolList[i].SetActive(false);
        }
    }

    public virtual void RemoveBullets(int count)
    {
        GameObject g;
        int deleteCount = Mathf.Max(poolList.Count - count, 0);
        for (int i = poolList.Count - 1; i > deleteCount; i--)
        {
            g = poolList[i];
            poolList.RemoveAt(i);
            GameObject.Destroy(g);
        }
    }
}
