﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonWithConfirm : MonoBehaviour {
    public bool onWarning = false;
    public float warningTime = 2f;
    public Color color1;
    public Color color2;
    public UnityEngine.UI.Image image;
    [SerializeField] public UnityEngine.Events.UnityEvent onConfirmed;

    public float warningTimer;

    public void Click()
    {
        if (onWarning)
        {
            onConfirmed.Invoke();
            image.color = color1;
            onWarning = false;
            return;
        }
        onWarning = true;
        warningTimer = warningTime;
    }

    public void Update()
    {
        if (onWarning) {
            warningTimer -= Time.unscaledDeltaTime;
            ImageAnimation(warningTimer);
            if (warningTimer <= 0)
            {
                image.color = color1;
                onWarning = false;
            }
        }
    }

    public void ImageAnimation(float timer)
    {
        image.color = Color.Lerp(color1, color2, Mathf.Sin((warningTime - timer) * 3f) * 0.5f + 0.5f);
    }

}
