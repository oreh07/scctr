﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : Shell
{
    [SerializeField]
    private int internal_PointCount = 10;

    [SerializeField]
    float lifetime = 0.1f;

    public Vector3 direction;
    public bool isHoming = false;

    Transform[] internalPoints;
    LineRenderer lineRenderer;

    public override void Init()
    {
        base.Init();
        internalPoints = new Transform[internal_PointCount];
        for (int i = 0; i < internal_PointCount; i++)
        {
            internalPoints[i] = new GameObject("internal_" + i).transform;
            internalPoints[i].SetParent(this.transform);
            internalPoints[i].localScale = Vector3.one;
        }

        lineRenderer = gameObject.AddComponent<LineRenderer>();
        lineRenderer.material = Resources.Load<Material>("Materials/rainbow");
    }

    public override void Setup()
    {
        base.Setup();

        lifetimeTimer = 0;

        Ray r = new Ray(this.transform.position, direction);
        RaycastHit hit;
        float endPointLength = 50f;
        if (Physics.Raycast(r, out hit, 100f, 1 << LayerMask.NameToLayer("Borders")))
        {
            endPointLength = Vector3.Distance(hit.point, this.transform.position) + 3f;
        }

        for (int i = 0; i < internal_PointCount; i++)
        {
            internalPoints[i].localPosition = new Vector3(0, 0, endPointLength * ((float)i / (internal_PointCount - 1)));
        }

        lineRenderer.positionCount = 0;
    }

    float lifetimeTimer = 0;
    public override void Update()
    {
        if (!isSleep)
        {
            lifetimeTimer += Time.deltaTime;
            if (lifetimeTimer > lifetime)
            {
                Turn(false);
                return;
            }

            if (isHoming)
            {
                Homing();
                DrawByCurves(internalPoints);
            }
            else
            {
                DrawLine();
            }

            CheckCollision();
        }
    }

    protected override bool ItsEnemy(GameObject g, ref Unit u)
    {
        u = g.GetComponentInParent<Unit>();
        if (!u)
        {
            return false;
        }
        string tag = u.gameObject.tag;
        return side == ShellSide.Player && u.gameObject.CompareTag("Enemy") ||
               side == ShellSide.Enemy && u.gameObject.CompareTag("Player");
    }

    void DrawByCurves(Transform[] points)
    {
        List<Vector3> pts = new List<Vector3>();

        pts.Add(points[0].position);

        float dl = 0;
        for (var i = 0; i < points.Length - 2; i++)
        {
            float dr = (points[i + 2].position.z - points[i].position.z) / 2;
            float a3 = dl + dr + 2 * (points[i].position.z - points[i + 1].position.z);
            float a2 = points[i + 1].position.z - a3 - dl - points[i].position.z;

            for (float t = 0; t <= 1; t += .25f)
            {
                float y = a3;
                y = y * t + a2;
                y = y * t + dl;
                y = y * t + points[i].position.z;
                pts.Add(new Vector3(points[i].position.x + t * (points[i + 1].position.x - points[i].position.x), 0, y));
            }
            dl = dr;
        }

        lineRenderer.positionCount = pts.Count;
        lineRenderer.SetPositions(pts.ToArray());

        for (var i = 0; i < pts.Count - 1; i++)
        {
            Debug.DrawLine(pts[i], pts[i + 1], Color.Lerp(Color.blue, Color.green, Random.Range(0f, 1f)), lifetime);
        }
    }

    void DrawLine()
    {
        lineRenderer.positionCount = 2;
        lineRenderer.SetPosition(0, internalPoints[0].position);
        lineRenderer.SetPosition(1, internalPoints[internalPoints.Length - 1].position);
    }

    void Homing()
    {
        const float distance = 9f;
        List<Bot> bots = BotController.Instance.bots;
        for (int i = 1; i < internal_PointCount - 1; i++)
        {
            for (int j = 0; j < bots.Count; j++)
            {
                if (Vector3.SqrMagnitude(internalPoints[i].position - bots[j].transform.position) < distance)
                {
                    internalPoints[i].position = bots[j].transform.position;
                    break;
                }
            }
        }
    }

    private void CheckCollision()
    {
        List<Unit> cols = new List<Unit>();

        Ray r;
        Unit u = null;

        for (int i = 0; i < internal_PointCount - 1; i++)
        {
            r = new Ray(internalPoints[i].position, internalPoints[i + 1].position - internalPoints[i].position);
            RaycastHit[] raycastHits = Physics.RaycastAll(r, 100f);
            foreach (RaycastHit rh in raycastHits)
                if (rh.collider != null)
                {
                    if (ItsEnemy(rh.collider.gameObject, ref u))
                    {
                        if (!cols.Contains(u))
                        {
                            cols.Add(u);
                        }
                    }
                }
        }
        foreach (Unit un in cols)
        {
            un.Hit(damage);
        }
    }

    float CheckDistance(Vector3 from, Vector3 to)
    {
        return 0f;
    }
}
