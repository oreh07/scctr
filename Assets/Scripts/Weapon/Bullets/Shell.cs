﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public enum ShellSide
{
    Player = 0,
    Enemy = 1,
    All = 2
}

public class Shell : MonoBehaviour
{
    protected BulletController _bc;
    protected BulletController bc
    {
        get
        {
            if (_bc == null)
            {
                _bc = BulletController.Instance;
            }
            return _bc;
        }
    }

    public float damage = 1f;
    public bool isSleep = true;
    public BulletType type;

    public ShellSide _side;
    public ShellSide side
    {
        get
        {
            return _side;
        }
        set
        {
            _side = value;
            this.gameObject.layer = LayerMask.NameToLayer(_side == ShellSide.Player ? "PlayerBullet" : "EnemyBullet");

            MaterialAnimation();
        }
    }

    protected MeshRenderer _meshRenderer;
    protected MeshRenderer meshRenderer
    {
        get
        {
            if (_meshRenderer == null)
            {
                _meshRenderer = GetComponentInChildren<MeshRenderer>();
            }
            return _meshRenderer;
        }
    }

    public virtual void Turn(bool on)
    {
        isSleep = !on;
        gameObject.SetActive(on);

        if (!on)
        {
            bc.Return(this);
        }
    }

    public virtual Shell Copy(Shell shell)
    {
        shell.damage = damage;
        return shell;
    }

    public virtual void Init() { }
    public virtual void Setup() { }
    protected virtual bool ItsEnemy(GameObject g, ref Unit u) { return false; }
    public virtual void Update()
    {
    }

    private void MaterialAnimation()
    {
        meshRenderer.material = (_side == ShellSide.Enemy) ? bc.enemyBulletMaterial : bc.playerBulletMaterial;

        Material mat = meshRenderer.material;
        mat.DOPause();

        if (side == ShellSide.Enemy)
        {
            mat.color = new Color(0.5f, 0, 0);
            mat.DOColor(new Color(1f, 0.5f, 0.5f), 0.3f).SetLoops(-1);
        }
    }
}
