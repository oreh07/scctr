﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Bullet : Shell
{
    [HideInInspector] public int pierce = 0;
    public bool isHoming = false;
    [HideInInspector] public float speed = 0.5f;
    public Vector3 movement;

    protected virtual void Awake()
    {
        Turn(false);
    }

    public override void Turn(bool on)
    {
        base.Turn(on);
    }

    public override Shell Copy(Shell shell)
    {
        (shell as Bullet).speed = speed;
        (shell as Bullet).pierce = pierce;
        return base.Copy(shell);
    }

    public override void Update() {
        if (!isSleep)
        {
            Homing();
            transform.position += movement * speed * Time.deltaTime;
            transform.rotation = Quaternion.LookRotation(movement, Vector3.up);
        }
    }

    protected override bool ItsEnemy(GameObject g, ref Unit u)
    {
        u = g.GetComponentInParent<Unit>();
        if (!u)
        {
            return false;
        }
        string tag = u.gameObject.tag;
        return side == ShellSide.Player && u.gameObject.CompareTag("Enemy") ||
               side == ShellSide.Enemy && u.gameObject.CompareTag("Player");
    }

    protected virtual void OnCollideWall()
    {
        Turn(false);
    }

    protected virtual void OnCollideEnemy(Unit u)
    {
        pierce--;
        if (pierce < 0)
        {
            bc.CreateGib(transform.position, u.transform.position);
            Turn(false);
        }
    }

    protected virtual void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("Wall"))
        {
            OnCollideWall();
        }

        Unit u = null;
        if (ItsEnemy(col.gameObject, ref u))
        {
            OnCollideEnemy(u);
        }
    }

    protected virtual void OnCollisionEnter(Collision col)
    {}

    float homingTimer = 0;
    const float homingReload = 0.15f;
    const float sqrHomingRange = 80f;
    const float homingSpeed = 0.5f;
    Vector3 homingVector = Vector3.zero;
    protected void Homing()
    {
        if (!isHoming)
        {
            return;
        }
        //movement = Quaternion.Lerp(transform.rotation, homingVector, Time.deltaTime) * movement;

        movement = Vector3.RotateTowards(movement, homingVector, homingSpeed * Time.deltaTime, 0);

        homingTimer += Time.deltaTime;
        if (homingTimer > homingReload)
        {
            homingTimer = 0f;

            float range = 0;
            float dangle = 0;
            Vector3 v;
            List<Bot> bots = BotController.Instance.bots;
            homingVector = transform.forward;
            for (int i = 0; i < bots.Count; i++)
            {
                //Debug.Log(i + " - " + bots[i].name);
                v = bots[i].transform.position - this.transform.position;
                range = Vector3.SqrMagnitude(v);
                if (range > sqrHomingRange)
                {
                    continue;
                }
                dangle = Quaternion.FromToRotation(transform.forward, v).eulerAngles.y; //Vector3.Angle(v, this.transform.forward);
                //print(dangle);

                if (dangle > 300 || dangle < 60)
                {
                    homingVector = Quaternion.Euler(0, dangle, 0) * transform.forward;
                    Debug.DrawLine(this.transform.position, this.transform.position + homingVector * Mathf.Sqrt(range), Color.white, homingReload);
                }
            }
        }
    }
}
