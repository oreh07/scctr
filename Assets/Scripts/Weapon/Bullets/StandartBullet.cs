﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandartBullet : Bullet
{
    protected override void OnCollideEnemy(Unit u)
    {
        u.Hit(damage);
        base.OnCollideEnemy(u);
    }
}
