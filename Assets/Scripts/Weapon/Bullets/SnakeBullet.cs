﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeBullet : Bullet {
    public float frequency;
    public float amplitude;

    private float startTime;

    public override void Setup()
    {
        base.Setup();
        startTime = Time.time;
    }

    public override void Update()
    {
        if (!isSleep)
        {
            Homing();
            transform.position += Quaternion.Euler(0, amplitude * Mathf.Sin((startTime - Time.time) * frequency), 0) * movement * speed * Time.deltaTime;
        }
    }

    public override Shell Copy(Shell shell)
    {
        (shell as SnakeBullet).frequency = frequency;
        (shell as SnakeBullet).amplitude = amplitude;
        return base.Copy(shell);
    }

    protected override void OnCollideEnemy(Unit u)
    {
        u.Hit(damage);
        base.OnCollideEnemy(u);
    }
}
