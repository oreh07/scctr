﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserShooter : Shooter
{
    private float shootTimer = 0;
    private List<Laser> lasers;

    public override bool isHoming
    {
        get
        {
            return base.isHoming;
        }
        set
        {
            base.isHoming = value;
            if (lasers != null)
            {
                foreach (Laser l in lasers)
                {
                    l.isHoming = value;
                }
            }
        }
    }

    public override void Init()
    {
        lasers = new List<Laser>();
        for (int i = 0; i < 6; i++)
        {
            lasers.Add(CreateLaserGameobject());
        }

    }

    private Laser CreateLaserGameobject()
    {
        GameObject o = new GameObject("laser" + lasers.Count);
        o.transform.SetParent(this.transform);
        o.transform.localRotation = Quaternion.identity;
        o.AddComponent<Laser>();
        o.GetComponent<Laser>().Init();
        o.GetComponent<Laser>().Turn(false);

        Laser l = o.GetComponent<Laser>();

        return l;
    }

    public override void Shoot(Vector3 direction, ShellSide side = ShellSide.Enemy)
    {
        Laser b;
        if (!isRealoding)
        {
            if (attacksPerShoot == 1)
            {
                b = Setup(side) as Laser;
                b.transform.position = this.transform.position;
                b.direction = direction;
                b.Setup();
            }
            else if (attacksPerShoot == 2)
            {
                b = Setup(side) as Laser;
                b.transform.position = this.transform.position + this.transform.right * 0.5f;
                b.direction = direction;
                b.Setup();

                b = Setup(side) as Laser;
                b.transform.position = this.transform.position - this.transform.right * 0.5f;
                b.direction = direction;
                b.Setup();
            }
            else if (attacksPerShoot == 3)
            {
                b = Setup(side) as Laser;
                b.transform.position = this.transform.position + this.transform.right * 0.66f;
                b.direction = Quaternion.Euler(0, 15, 0) * direction;
                b.Setup();

                b = Setup(side) as Laser;
                b.transform.position = this.transform.position;
                b.direction = direction;
                b.Setup();

                b = Setup(side) as Laser;
                b.transform.position = this.transform.position - this.transform.right * 0.66f;
                b.direction = Quaternion.Euler(0, -15, 0) * direction;
                b.Setup();
            }

            if (reloadTime > 0f)
            {
                isRealoding = true;
                shootTimer = reloadTime;
            }
        }
    }

    void Update()
    {
        if (shootTimer >= 0)
        {
            shootTimer -= Time.deltaTime;
            if (shootTimer <= 0)
            {
                isRealoding = false;
            }
        }
    }

    public override Shell Setup(ShellSide side)
    {
        Laser l = lasers[0];
        for (int i = 1; i < lasers.Count; i++)
        {
            if (lasers[i].isSleep)
            {
                l = lasers[i];
            }
        }

        l.side = side;
        l.Turn(true);
        return l;
    }
}
