﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class Shooter : MonoBehaviour, IStatRestoreItem
{
    protected BulletController bc;
    public abstract void Shoot(Vector3 direction, ShellSide side = ShellSide.Enemy);
    public abstract Shell Setup(ShellSide shell);
    
    public int attacksPerShoot = 1;
    public float damageMult = 1f;
    public float reloadTime = 0f;

    private bool _isHoming;
    public virtual bool isHoming {
        set
        {
            _isHoming = value;
        }
        get
        {
            return _isHoming;
        }
    }

    private StatRestoreController _stats;
    public StatRestoreController stats
    {
        get
        {
            if (_stats == null)
            {
                _stats = new StatRestoreController();
            }
            return _stats;
        }
    }

    protected bool isRealoding = false;
    public BulletType type;

    void Start()
    {
        bc = BulletController.Instance;
    }

    public void Copy(Shooter sh)
    {
        sh.attacksPerShoot = attacksPerShoot;
        sh.damageMult = damageMult;
        sh.reloadTime = reloadTime;
        sh.isHoming = isHoming;
        sh.isRealoding = isRealoding;
    }

    public virtual void Init() {}

    public virtual void SaveDefaultStats()
    {
        Debug.LogFormat("<color=grey>unit: [{0}] saving default stats</color>", name);

        stats.AddStat("attackPerShots", attacksPerShoot);
        stats.AddStat("damageMult", damageMult);
        stats.AddStat("reloadTime", reloadTime);
        stats.AddStat("type", type.ToString());
    }
    public virtual void RestoreDefaultStats()
    {
        Debug.LogFormat("<color=grey>unit: [{0}] restoring default stats</color>", name);

        attacksPerShoot = (int)stats.GetStatFloat("attackPerShots");
        damageMult = stats.GetStatFloat("damageMult");
        reloadTime = stats.GetStatFloat("reloadTime");
        type = BulletType.Standart;
    }
    public virtual void DestroyStats()
    {
        Debug.LogFormat("<color=grey>unit: [{0}] destroy default stats</color>", name);
    }
}
