﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultipleShooter : Shooter
{
    public Shooter[] shooters;
    public override void Init()
    {
        if(shooters == null || shooters.Length == 0)
        {
            shooters = GetComponentsInChildren<Shooter>();
        }
    }

    public Shooter GetShooter(string name)
    {
        for (int i = 0; i < shooters.Length; i++)
        {
            if (shooters[i].name.CompareTo(name) == 0)
            {
                return shooters[i];
            }
        }
        return shooters[0];
    }

    public override Shell Setup(ShellSide shell)
    {
        return null;
    }

    public override void Shoot(Vector3 direction, ShellSide side = ShellSide.Enemy)
    {
        foreach (Shooter sh in shooters)
        {
            sh.Shoot(direction, side);
        }
    }
    public void Shoot(Vector3 direction, string shooter = "", ShellSide side = ShellSide.Enemy)
    {
        Shooter sh = GetShooter(shooter);
        sh.Shoot(direction, side);
    }
}
