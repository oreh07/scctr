﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BulletType
{
    Money = -3,
    Gib = -2,
    None = -1,
    Standart = 0,
    Snake,
    Flower,
}


public class BulletController : MonoBehaviour {
    public static BulletController Instance;

    [System.Serializable]
    private struct BulletPool
    {
        public BulletType type;
        public GameObject bulletPrefab;
    }
    
    public Material enemyBulletMaterial;
    public Material playerBulletMaterial;

    [SerializeField]
    private List<BulletPool> prefabs;
    private ShellPool<StandartBullet> standart;
    private ShellPool<SnakeBullet> snake;
    private ShellPool<FlowerBullet> flower;
    private ShellPool<Gib> gibs;
    private ShellPool<Money> money;

    void Awake()
    {
        Instance = this;

        gibs = new ShellPool<Gib>();
        standart = new ShellPool<StandartBullet>();
        snake = new ShellPool<SnakeBullet>();
        flower = new ShellPool<FlowerBullet>();
        money = new ShellPool<Money>();

        MenuController.onLevelChanged += OnLevelChanged;
    }

    void Start()
    {
        standart.Init(this.transform, prefabs.Find(x => { return x.type == BulletType.Standart; }).bulletPrefab, 20);
        snake.Init(this.transform, prefabs.Find(x => { return x.type == BulletType.Snake; }).bulletPrefab, 5);
        flower.Init(this.transform, prefabs.Find(x => { return x.type == BulletType.Flower; }).bulletPrefab, 5);

        money.Init(this.transform, prefabs.Find(x => { return x.type == BulletType.Money; }).bulletPrefab, 10);
    }

    public void GetMoney(Vector3 position, Bot bot)
    {
        for(int i = 0; i < bot.moneyReward; i++)
        {
            Money m = money.GetBullet();
            m.Setup();
            m.Turn(true);
            m.transform.position = position;
            Vector3 d = Quaternion.Euler(Random.insideUnitSphere * 45f) * Vector3.up;
            d.y = 0;
            m.speed = d * 5f;
            //m.GetComponent<Rigidbody>().AddForce(d * 5f, ForceMode.Impulse);
        }

    }
    public void CreateGib(Vector3 position, Vector3 enemy)
    {
        Gib g = gibs.GetBullet();
        g.Turn(true);
        g.transform.position = position;
        g.GetComponent<Rigidbody>().AddForce((position - enemy).normalized * 0.2f, ForceMode.Impulse);
    }

    public Shell GetBullet(BulletType t)
    {
        switch (t) {
            case BulletType.Standart: return standart.GetBullet();
            case BulletType.Snake: return snake.GetBullet();
            case BulletType.Flower: return flower.GetBullet();
        }
        Debug.LogWarning("can't find bullet for type [" + t.ToString() + "]");
        return standart.GetBullet();
    }

    public void Return(Shell b) {}
    
    public void ClearBullets()
    {
        standart.DestroyAllBullets();   standart.RemoveBullets((int)(standart.Count * 0.2f));
        snake.DestroyAllBullets();      snake.RemoveBullets((int)(snake.Count * 0.2f));
        gibs.DestroyAllBullets();       gibs.RemoveBullets((int)(snake.Count * 0.5f));
    }

    private void OnLevelChanged(Level levelContainer)
    {
        gibs.Init(levelContainer.transform, prefabs.Find(x => { return x.type == BulletType.Gib; }).bulletPrefab, 20);
    }
}

