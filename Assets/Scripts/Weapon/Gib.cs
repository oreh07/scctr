﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gib : Shell
{
    public float lifetime = 5f;
    public float lifetimer = 0f;

    public override void Turn(bool on)
    {
        base.Turn(on);
        if (on)
        {
            lifetimer = 0;
        }
    }

    public override void Update()
    {
        base.Update();
        if (!isSleep)
        {
            lifetimer += Time.deltaTime;
            if (lifetimer > lifetime)
            {
                Turn(false);
            }
        }
    }
}
