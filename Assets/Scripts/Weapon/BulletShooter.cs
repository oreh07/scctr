﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletShooter : Shooter
{
    private float shootTimer = 0;
    public int pierce = 0;
    public float bulletSpeedMultiple = 1;

    public override void Init(){}

    public override void Shoot(Vector3 direction, ShellSide side = ShellSide.Enemy)
    {
        Bullet b;
        if (!isRealoding)
        {
            if (attacksPerShoot == 1)
            {
                b = Setup(side) as Bullet;
                b.transform.position = this.transform.position;
                b.movement = direction;
            }
            else if(attacksPerShoot == 2)
            {
                b = Setup(side) as Bullet;
                b.transform.position = this.transform.position + this.transform.right * 0.5f;
                b.movement = direction;

                b = Setup(side) as Bullet;
                b.transform.position = this.transform.position - this.transform.right * 0.5f;
                b.movement = direction;
            }
            else if (attacksPerShoot == 3)
            {
                b = Setup(side) as Bullet;
                b.transform.position = this.transform.position + this.transform.right * 0.66f;
                b.movement = Quaternion.Euler(0, 15, 0) * direction;

                b = Setup(side) as Bullet;
                b.transform.position = this.transform.position;
                b.movement = direction;

                b = Setup(side) as Bullet;
                b.transform.position = this.transform.position - this.transform.right * 0.66f;
                b.movement = Quaternion.Euler(0, -15, 0) * direction;
            }
            else if (attacksPerShoot == 6)
            {
                b = Setup(side) as Bullet;
                b.transform.position = this.transform.position + this.transform.right * 0.66f;
                b.movement = Quaternion.Euler(0, 15, 0) * direction;

                b = Setup(side) as Bullet;
                b.transform.position = this.transform.position + this.transform.right * 0.45f;
                b.movement = Quaternion.Euler(0, 8, 0) * direction;

                b = Setup(side) as Bullet;
                b.transform.position = this.transform.position + this.transform.right * 0.2f;
                b.movement = Quaternion.Euler(0, 2, 0) * direction;

                b = Setup(side) as Bullet;
                b.transform.position = this.transform.position - this.transform.right * 0.2f;
                b.movement = Quaternion.Euler(0, -2, 0) * direction;

                b = Setup(side) as Bullet;
                b.transform.position = this.transform.position - this.transform.right * 0.45f;
                b.movement = Quaternion.Euler(0, -8, 0) * direction;

                b = Setup(side) as Bullet;
                b.transform.position = this.transform.position - this.transform.right * 0.66f;
                b.movement = Quaternion.Euler(0, -15, 0) * direction;
            }

            if (reloadTime > 0f)
            {
                isRealoding = true;
                shootTimer = reloadTime;
            }
        }
    }

    void Update()
    {
        if (shootTimer >= 0)
        {
            shootTimer -= Time.deltaTime;
            if (shootTimer <= 0)
            {
                isRealoding = false;
            }
        }
    }

    public override Shell Setup(ShellSide side)
    {
        Bullet b = bc.GetBullet(type) as Bullet;
        b.Setup();
        b.damage *= damageMult;
        b.side = side;
        b.Turn(true);
        b.pierce = pierce;
        b.isHoming = isHoming;
        b.speed *= bulletSpeedMultiple;
        return b;
    }

    public override void SaveDefaultStats()
    {
        base.SaveDefaultStats();
        stats.AddStat("pierce", pierce);
        stats.AddStat("bulletSpeedMultiple", bulletSpeedMultiple);

    }
    public override void RestoreDefaultStats()
    {
        base.RestoreDefaultStats();
        pierce = (int)stats.GetStatFloat("pierce");
        bulletSpeedMultiple = stats.GetStatFloat("bulletSpeedMultiple");
    }
}
