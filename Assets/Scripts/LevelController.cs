﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LevelType
{
    None = 0,
    EpicenterOfTheCity = 1,
    DeepDeepMarket = 2,
    EndlessCarousel = 3,
    WorldInAStorm = 4,
    DarkForest = 5,
    CountlessRelatives = 6,
    RoyalDungeon = 7
}

public class LevelController : MonoBehaviour
{
    public static LevelController Instance;
    public Transform levelContainer;
    public List<Level> levels;

    [HideInInspector] public Level currentLevel;


    void Awake()
    {
        Instance = this;
    }
    
    public void LoadLevel(LevelType level)
    {
        print("load level " + level.ToString());
        GameObject levelPrefab = levels.Find(x => x.levelName == level.ToString()).gameObject;
        GameObject lvl = Instantiate(levelPrefab, levelContainer);
        lvl.transform.position = Vector3.zero;

        currentLevel = lvl.GetComponent<Level>();
    }

    public void DestroyWorld()
    {
        //foreach (Transform t in levelContainer)
        //{
        //    Destroy(t.gameObject);
        //}
        print("destroy");
        Destroy(currentLevel.gameObject);

    }
}
