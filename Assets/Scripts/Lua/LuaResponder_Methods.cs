﻿using UnityEngine;
using UniLua;

public partial class LuaResponder : MonoBehaviour
{

    #region lib functions
    #region other
    protected virtual int luaTrace(ILuaState s)
    {
        Debug.Log("ltrace: " + s.L_CheckString(1));
        return 1;
    }
    protected virtual int setVariable(ILuaState s)
    {
        string key = s.L_CheckString(1);

        if (s.Type(2) == UniLua.LuaType.LUA_TSTRING)
        {
            if (responderStringDict.ContainsKey(key))
            {
                responderStringDict.Remove(key);
            }
            responderStringDict.Add(key, s.L_CheckString(2));
        }
        else
        {
            if (responderNumberDict.ContainsKey(key))
            {
                responderNumberDict.Remove(key);
            }
            responderNumberDict.Add(key, s.L_CheckNumber(2));
        }

        return 1;
    }
    protected virtual int getVariable(ILuaState s)
    {
        //Debug.Log("  getVariable "+s.L_CheckString(1));
        string key = s.L_CheckString(1);

        if (responderStringDict.ContainsKey(key))
        {
            s.PushString(responderStringDict[key]);
        }
        else if (responderNumberDict.ContainsKey(key))
        {
            s.PushNumber(responderNumberDict[key]);
        }
        else
        {
            s.PushInteger(0);
        }
        return 1;
    }
    #endregion
    #region actions
    protected virtual int getBotCount(ILuaState s)
    {
        s.PushInteger(BotController.Instance.bots.Count);
        return 1;
    }
    protected virtual int getPosition(ILuaState s)
    {
        return 1;
    }
    protected virtual int initBoss(ILuaState s)
    {
        return 1;
    }
    protected virtual int setInvulnerableLua(ILuaState s)
    {
        return 1;
    }
    protected virtual int getUnitHealth(ILuaState s)
    {
        return 1;
    }
    protected virtual int getRangom(ILuaState s)
    {
        s.PushNumber(Random.Range(0f, 1f));
        return 1;
    }
    protected virtual int endOfLevel(ILuaState s)
    {
        MenuController.Instance.LevelEnd();
        return 1;
    }
    protected virtual int moveSpeed(ILuaState s)
    {
        return 1;
    }
    protected virtual int getRotationFrom(ILuaState s)
    {
        s.PushNumber(0f);
        return 1;
    }
    protected virtual int getRotation(ILuaState s)
    {
        s.PushNumber(0f);
        return 1;
    }
    protected virtual int shootFromLua(ILuaState s)
    {
        return 1;
    }
    protected virtual int shootLua(ILuaState s)
    {
        return 1;
    }
    protected virtual int moveToSaved(ILuaState s)
    {
        return 1;
    }
    protected virtual int moveToPoint(ILuaState s)
    {
        return 1;
    }
    protected virtual int moveToEnd(ILuaState s)
    {
        return 1;
    }
    protected virtual int spawn(ILuaState s)
    {
        return 1;
    }
    protected virtual int start(ILuaState s)
    {
        return 1;
    }
    protected virtual int setPoint(ILuaState s)
    {
        return 1;
    }
    protected virtual int setPointNamed(ILuaState s)
    {
        return 1;
    }
    protected virtual int end(ILuaState s)
    {
        return 1;
    }
    protected virtual int loadScriptLua(ILuaState s)
    {
        loadScript(s.L_CheckString(1));
        return 1;
    }
    protected virtual int loadScriptUnitLua(ILuaState s)
    {
        string unitName = s.L_CheckString(1);
        Unit unit = FindUnitByName(unitName);
        if (unit != null)
        {
            unit.LoadScript(s.L_CheckString(2));
        }
        return 1;
    }
    protected virtual int waiting(ILuaState s)
    {
        StartCoroutine(StartWaiting(s.L_CheckNumber(1)));
        return s.YieldK(s.GetTop(), 0, null);
    }
    #endregion
    #endregion

    protected virtual int getShooters(ILuaState s)
    {
        s.PushInteger(0);
        s.PushInteger(1);
        s.PushInteger(3);
        s.PushInteger(2);
        return 1;
    }

    Unit FindUnitByName(string unitName)
    {
        Unit[] npcs = FindObjectsOfType<Unit>();
        for (int i = 0; i < npcs.Length; i++)
        {
            if (npcs[i].name == unitName)
            {
                return npcs[i];
            }
        }
        return null;
    }
}
