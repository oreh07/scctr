﻿using UnityEngine;
using UniLua;

public partial class LuaResponder_moon : MonoBehaviour
{

    #region lib functions
    #region other
    protected virtual int SetStringVariable(string key, string value)
    {
        if (responderStringDict.ContainsKey(key))
        {
            responderStringDict.Remove(key);
        }
        responderStringDict.Add(key, value);
        return 1;
    }

    protected virtual int SetNumberVariable(string key, double value)
    {
        if (responderNumberDict.ContainsKey(key))
        {
            responderNumberDict.Remove(key);
        }
        responderNumberDict.Add(key, value);
        return 1;
    }

    protected virtual string GetStringVariable(string key)
    {
        if (responderStringDict.ContainsKey(key))
        {
            return responderStringDict[key];
        }
        LogWarning(string.Format("cant find number variable named:[{0}]", key));
        return string.Empty;
    }

    protected virtual double GetNumberVariable(string key)
    {
        if (responderNumberDict.ContainsKey(key))
        {
            return responderNumberDict[key];
        }
        LogWarning(string.Format("cant find string variable named:[{0}]", key));
        return -1;
    }
    #endregion
    #region actions
    protected virtual int getBotCount(ILuaState s)
    {
        s.PushInteger(BotController.Instance.bots.Count);
        return 1;
    }
    protected virtual int getPosition(ILuaState s)
    {
        return 1;
    }
    protected virtual int initBoss(ILuaState s)
    {
        return 1;
    }
    protected virtual int setInvulnerableLua(ILuaState s)
    {
        return 1;
    }
    protected virtual int getUnitHealth(ILuaState s)
    {
        return 1;
    }
    protected virtual float GetRandom(float min = 0, float max = 1)
    {
        return Random.Range(min, max);
    }
    protected virtual int endOfLevel(ILuaState s)
    {
        MenuController.Instance.LevelEnd();
        return 1;
    }
    protected virtual int LevelCreateObject(string name) { return 1; }
    protected virtual int LevelDestroyObject(string name) { return 1; }
    protected virtual int LevelMoveElement(string name, string vector, float time) { return 1; }
    protected virtual int LevelRotateElement(string name, string vector, float time) { return 1; }
    protected virtual int LevelMoveMap(float speed) { return 1; }
    protected virtual int LevelShowElement(string name, float time) { return 1; }
    protected virtual int LevelHideElement(string name, float time) { return 1; }
    protected virtual int LevelShakePositionElement(string name, float time, float strenth, int vibrato) { return 1; }
    protected virtual int LevelShakeRotationElement(string name, float time, float strenth, int vibrato) { return 1; }
    protected virtual int LevelSetText(string name, string text) { return 1; }
    protected virtual int getRotationFrom(ILuaState s)
    {
        s.PushNumber(0f);
        return 1;
    }
    protected virtual int getRotation(ILuaState s)
    {
        s.PushNumber(0f);
        return 1;
    }
    protected virtual int shootFromLua(ILuaState s)
    {
        return 1;
    }
    protected virtual int shootLua(ILuaState s)
    {
        return 1;
    }
    protected virtual int moveToSaved(ILuaState s)
    {
        return 1;
    }
    protected virtual int moveToPoint(float x, float y)
    {
        return 1;
    }
    protected virtual int moveToEnd(ILuaState s)
    {
        return 1;
    }
    protected virtual int Spawn(string name)
    {
        return 1;
    }
    protected virtual int start(ILuaState s)
    {
        return 1;
    }
    protected virtual int setPoint(ILuaState s)
    {
        return 1;
    }
    protected virtual int setPointNamed(ILuaState s)
    {
        return 1;
    }
    protected virtual int end(ILuaState s)
    {
        return 1;
    }
    protected int PlayerControl(bool value)
    {
        PlayerController.Instance.AllowControl = value;
        return 1;
    }
    protected virtual int LoadScriptFuncLua(string name, string function)
    {
        LoadScript(name);
        if (function != null && function.Length > 0)
        {
            CallFunction(function);
        }
        return 1;
    }
    protected virtual int LoadScriptCorLua(string name, string function)
    {
        LoadScript(name);
        if (function != null && function.Length > 0)
        {
            CallCoroutine(function);
        }
        return 1;
    }
    protected virtual int loadScriptUnitLua(ILuaState s)
    {
        string unitName = s.L_CheckString(1);
        Unit unit = FindUnitByName(unitName);
        if (unit != null)
        {
            //unit.loadScript(s.L_CheckString(2));
        }
        return 1;
    }
    protected virtual int waiting(ILuaState s)
    {
        //StartCoroutine(StartWaiting(s.L_CheckNumber(1)));
        return s.YieldK(s.GetTop(), 0, null);
    }


    protected virtual int Waiting(float a)
    {
        Coroutines.Start(StartWaiting(a));
        return 0;
    }
    #endregion
    #endregion

    protected virtual int getShooters(ILuaState s)
    {
        s.PushInteger(0);
        s.PushInteger(1);
        s.PushInteger(3);
        s.PushInteger(2);
        return 1;
    }

    Unit FindUnitByName(string unitName)
    {
        Unit[] npcs = FindObjectsOfType<Unit>();
        for (int i = 0; i < npcs.Length; i++)
        {
            if (npcs[i].name == unitName)
            {
                return npcs[i];
            }
        }
        return null;
    }
}
