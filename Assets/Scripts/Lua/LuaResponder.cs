﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UniLua;

public partial class LuaResponder : MonoBehaviour
{
    protected string luaScript;
    protected ILuaState luaState;
    protected ThreadStatus luaStatus;
    protected ILuaState threadLuaState;

    public static Dictionary<string, string> responderStringDict = new Dictionary<string, string>();
    public static Dictionary<string, double> responderNumberDict = new Dictionary<string, double>();

    protected string type;

    protected void loadScript(string name)
    {
        string url = string.Concat("lua/", type, "/", name); //, ".lua"
        //print("Load lua script: " + url);

        TextAsset targetFile = Resources.Load<TextAsset>(url);
        string luaText = "";
        try
        {
            luaText = targetFile.text;
        }
        catch (System.Exception ex)
        {
            Debug.LogError("loading file exception: " + ex.Message);
            Debug.LogError("url: " + url);
            return;
        }

        if (luaText.Length > 0)
        {
            if (luaState != null)
            {
                //print(threadLuaState.GetTop());
                threadLuaState.Pop(threadLuaState.GetTop());
            }

            luaState = LuaAPI.NewState();
            luaState.L_OpenLibs();
            luaState.L_RequireF("libs", libs, true);

            luaScript = luaText;

            threadLuaState = luaState.NewThread();
            luaStatus = threadLuaState.L_LoadString(luaScript);

            if (luaStatus != ThreadStatus.LUA_OK)
            {
                Debug.LogError(url + " not loaded. Status:" + luaStatus.ToString());
            }

            threadLuaState.Resume(null, 0);
        }
        else
        {
            Debug.LogWarning("file not found " + url);
            luaState = null;
            threadLuaState = null;
        }
    }

    IEnumerator StartWaiting(double time)
    {
        yield return new WaitForSeconds((float)time);
        threadLuaState.Resume(null, 0);
    }

    public void LuaResume()
    {
        threadLuaState.Resume(null, 0);
    }

    protected int libs(ILuaState lua)
    {
        var define = new NameFuncPair[]{

            //dialogue
            new NameFuncPair("loadScript", loadScriptLua),
            new NameFuncPair("loadScriptUnit", loadScriptUnitLua),

            //test
            new NameFuncPair("getShooters", getShooters),
            
            //actions
            new NameFuncPair("getBotCount", getBotCount),
            new NameFuncPair("getPosition", getPosition),
            new NameFuncPair("initBoss", initBoss),
            new NameFuncPair("setInvulnerable", setInvulnerableLua),
            new NameFuncPair("getUnitHealth", getUnitHealth),
            new NameFuncPair("getRandom", getRangom),
            new NameFuncPair("endOfLevel", endOfLevel),
            new NameFuncPair("moveSpeed", moveSpeed),
            new NameFuncPair("getRotationFrom", getRotationFrom),
            new NameFuncPair("getRotation", getRotation),
            new NameFuncPair("shootFrom", shootFromLua),
            new NameFuncPair("shoot", shootLua),
            new NameFuncPair("moveToSaved", moveToSaved),
            new NameFuncPair("moveToPoint", moveToPoint),
            new NameFuncPair("moveToEnd", moveToEnd),
            new NameFuncPair("spawn", spawn),
            new NameFuncPair("startPoint", start),
            new NameFuncPair("setPoint", setPoint),
            new NameFuncPair("setPointNamed", setPointNamed),
            new NameFuncPair("endPoint", end),
            new NameFuncPair("waiting", waiting),
            
            //items
            new NameFuncPair("waiting", waiting),

            //other
            new NameFuncPair("trace", luaTrace),
            new NameFuncPair("setVariable", setVariable),
            new NameFuncPair("getVariable", getVariable),
        };

        lua.L_NewLib(define);
        return 1;
    }
}