﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using System;
using System.Collections;
using System.Collections.Generic;
using MoonSharp.Interpreter;

[MoonSharpUserData]
class Test
{
    List<int> array = new List<int>();
    public void List(int v)
    {
        array.Add(v);
    }
    public void Print()
    {
        foreach (var s in array)
        {
            UnityEngine.Debug.Log(s);
        }
    }
}

public partial class LuaResponder_moon : MonoBehaviour
{
    protected LuaScript lua;
    protected string ScriptName { get; private set; }

    public static Dictionary<string, string> responderStringDict = new Dictionary<string, string>();
    public static Dictionary<string, double> responderNumberDict = new Dictionary<string, double>();

    protected string type;

    public void LoadScript(string name)
    {
        ScriptName = name;
        string url = string.Concat("lua/", type, "/", name);
        //print("Load lua script: " + url);
        lua = LuaScript.CreateFunction(name, FilesUtil.LoadScript(url));
        
        PrepareFunctions(lua);
    }

    protected void CallFunction(string name)
    {
        if (lua == null) return;
        lua.CallFunction(name);
    }

    protected double CallFunctionNumber(string name, params object[] args)
    {
        if (lua == null) return 0;
        return lua.CallFunctionNumber(name, args);
    }

    protected string CallFunctionString(string name, params object[] args)
    {
        if (lua == null) return string.Empty;
        return lua.CallFunctionString(name, args);
    }
    
    protected void CallCoroutine(string name, params object[] args)
    {
        if (lua == null) return;
        StartCoroutine(LuaCoroutine(lua.CallCoroutine(name, args)));
    }

    IEnumerator LuaCoroutine(DynValue coroutine)
    {
        int frames = 0;
        int counter = 0;

        do
        {
            if(frames <= counter)
            {
                DynValue x = coroutine.Coroutine.Resume();
                if (x.IsVoid()) yield break;

                frames = (int)x.Number;
                counter = 0;
            }

            counter++;
            yield return null;
        }
        while (true);
    }

    private void PrepareFunctions(LuaScript lua)
    {
        try
        {
            lua.Script.Globals["lCreateObject"] = (Func<string, int>)LevelCreateObject;
            lua.Script.Globals["lDestroyObject"] = (Func<string, int>)LevelDestroyObject;
            lua.Script.Globals["lMoveElement"] = (Func<string, string, float, int>)LevelMoveElement;
            lua.Script.Globals["lRotateElement"] = (Func<string, string, float, int>)LevelRotateElement;
            lua.Script.Globals["lMoveMap"] = (Func<float, int>)LevelMoveMap;
            lua.Script.Globals["lShowElement"] = (Func<string, float, int>)LevelShowElement;
            lua.Script.Globals["lHideElement"] = (Func<string, float, int>)LevelHideElement;
            lua.Script.Globals["lShakePositionElement"] = (Func<string, float, float, int, int>)LevelShakePositionElement;
            lua.Script.Globals["lShakeRotationElement"] = (Func<string, float, float, int, int>)LevelShakeRotationElement;
            lua.Script.Globals["lSetText"] = (Func<string, string, int>)LevelSetText;
            lua.Script.Globals["lSpawn"] = (Func<string, int>)Spawn;

            lua.Script.Globals["PlayerControl"] = (Func<bool, int>)PlayerControl;
            lua.Script.Globals["SetStringVariable"] = (Func<string, string, int>)SetStringVariable;
            lua.Script.Globals["SetNumberVariable"] = (Func<string, double, int>)SetNumberVariable;
            lua.Script.Globals["GetStringVariable"] = (Func<string, string>)GetStringVariable;
            lua.Script.Globals["GetNumberVariable"] = (Func<string, double>)GetNumberVariable;
            lua.Script.Globals["LoadScriptFunc"] = (Func<string, string, int>)LoadScriptFuncLua;
            lua.Script.Globals["LoadScriptCor"] = (Func<string, string, int>)LoadScriptCorLua;
            lua.Script.Globals["Waiting"] = (Func<float, int>)Waiting;

            lua.Script.Globals["GetRandom"] = (Func<float, float, float>)GetRandom;

        }
        catch (Exception ex)
        {
            Debug.LogError("can't prepare functions in " + lua.Name + "" + ex.Message);
        }
    }

    IEnumerator StartWaiting(double time)
    {

        yield return new WaitForSeconds((float)time);



    }

    public void LuaResume()
    {
    }

    protected void LogWarning(string message)
    {
        Debug.LogFormat("{0} [<color=yellow>LuaWarning</color>]:{1} {2}", Time.frameCount, ScriptName, message);
    }

    protected void LogError(string message)
    {
        Debug.LogFormat("{0} [<color=red>LuaError</color>]:{1} {2}", Time.frameCount, ScriptName, message);
    }

    //protected int libs(ILuaState lua)
    //{
    //    var define = new NameFuncPair[]{

    //        //dialogue
    //        new NameFuncPair("loadScript", loadScriptLua),
    //        new NameFuncPair("loadScriptUnit", loadScriptUnitLua),

    //        //test
    //        new NameFuncPair("getShooters", getShooters),

    //        //actions
    //        new NameFuncPair("getBotCount", getBotCount),
    //        new NameFuncPair("getPosition", getPosition),
    //        new NameFuncPair("initBoss", initBoss),
    //        new NameFuncPair("setInvulnerable", setInvulnerableLua),
    //        new NameFuncPair("getUnitHealth", getUnitHealth),
    //        new NameFuncPair("getRandom", getRangom),
    //        new NameFuncPair("endOfLevel", endOfLevel),
    //        new NameFuncPair("moveSpeed", moveSpeed),
    //        new NameFuncPair("getRotationFrom", getRotationFrom),
    //        new NameFuncPair("getRotation", getRotation),
    //        new NameFuncPair("shootFrom", shootFromLua),
    //        new NameFuncPair("shoot", shootLua),
    //        new NameFuncPair("moveToSaved", moveToSaved),
    //        new NameFuncPair("moveToPoint", moveToPoint),
    //        new NameFuncPair("moveToEnd", moveToEnd),
    //        new NameFuncPair("spawn", spawn),
    //        new NameFuncPair("startPoint", start),
    //        new NameFuncPair("setPoint", setPoint),
    //        new NameFuncPair("setPointNamed", setPointNamed),
    //        new NameFuncPair("endPoint", end),
    //        new NameFuncPair("waiting", waiting),

    //        //items
    //        new NameFuncPair("waiting", waiting),

    //        //other
    //        new NameFuncPair("trace", luaTrace),
    //        new NameFuncPair("setVariable", setVariable),
    //        new NameFuncPair("getVariable", getVariable),
    //    };

    //    lua.L_NewLib(define);
    //    return 1;
    //}
}