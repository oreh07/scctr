﻿using MoonSharp.Interpreter;
using System.Collections.Generic;


public class TestLuaReader : LuaResponder_moon
{
    void Start()
    {
        UserData.RegisterAssembly();

        type = "Test";
        LoadScript("level");

        //Debug.Log(CallNumber("fact", 5));
        //Debug.Log(CallString("name"));
        //CallFunction("variables");
        //CallFunctionString("loadScript");
        //CallFunctionString("anotherAction");
        //CallCoroutine("waiting");
        //CallFunctionNumber("random");
        //CallFunction("callByName");

        CallFunction("class");
        CallFunction("class");
    }

    void Update()
    {
        
    }
}
