﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthController : MonoBehaviour {
    public static HealthController Instance;
    private PlayerController pc;

    public bool upd;

    [Header("refs")]
    public Image[] images;
    public Sprite fullHearth;
    //public Sprite halfHearth;
    public Sprite emptyHearth;

    public float health
    {
        get
        {
            return pc.player.healthValue;
        }
    }

    void Awake()
    {
        Instance = this;
        pc = PlayerController.Instance;
        UpdateHealth();
    }

    public void Hit(float atHealth)
    {
        // particle
    }

    public void Heal(float atHealth)
    {
        // particle
    }

    public void UpdateHealth()
    {
        int max = Mathf.RoundToInt(pc.player.healthMax);
        int current = Mathf.RoundToInt(pc.player.healthValue);

        for(int i = 0; i < images.Length; i++)
        {
            //images[i].gameObject.SetActive(i < max / 2);
            images[i].gameObject.SetActive(i < max);
        }
        for(int i = 0; i < images.Length; i++)
        {
            images[i].sprite = (i < current) ? (emptyHearth) : (fullHearth);
        }

        //for (int i = 0; i < max / 2; i++)
        //{
        //    if (i * 2 + 2 <= current)
        //    {
        //        images[i].sprite = fullHearth;
        //    }
        //    else if (i * 2 + 1 == current)
        //    {
        //        images[i].sprite = halfHearth;
        //    }
        //    else
        //    {
        //        images[i].sprite = emptyHearth;
        //    }
        //}

    }
}
