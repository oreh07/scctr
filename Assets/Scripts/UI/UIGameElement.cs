﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIGameElement : GameElement
{
    private CanvasGroup _canvas;
    private CanvasGroup Canvas
    {
        get
        {
            if (_canvas == null)
            {
                _canvas = GetComponentInChildren<CanvasGroup>();
            }
            return _canvas;
        }
    }

    private float CanvasAlpha
    {
        get
        {
            return Canvas.alpha;
        }
        set
        {
            base.SetActive(value > 0, 0);
            Canvas.alpha = value;
        }
    }

    private Text _text;
    private Text Text
    {
        get
        {
            if (_text == null)
            {
                _text = GetComponentInChildren<Text>();
            }
            return _text;
        }
    }

    public override void SetActive(bool active, float time)
    {
        if (time == 0)
        {
            base.SetActive(active, time);
        }
        else
        {
            Canvas.alpha = active ? 0f : 1f;
            DOTween.To(() => CanvasAlpha, x => CanvasAlpha = x, active ? 1f : 0f, time);
        }
    }

    public void SetText(string text)
    {
        if(Text != null)
        {
            Text.text = text;
        }
        else
        {
            Debug.LogError("can't find text named [" + Name + "] with text [" + text + "]");
        }
    }
}