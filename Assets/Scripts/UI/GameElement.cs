﻿using UnityEngine;

public class GameElement : MonoBehaviour
{
    public static GameElement GetElement(string name)
    {
        var els = Resources.FindObjectsOfTypeAll<GameElement>();
        foreach(GameElement el in els)
        {
            if (el.Name == name)
            {
                return el;
            }
        }
        Debug.LogError("can't find GameElement named: [" + name + "]");
        return null;
    }

    [SerializeField] public string Name;

    public virtual void SetActive(bool active, float time)
    {
        gameObject.SetActive(active);
    }
}
