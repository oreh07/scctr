﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossHealthController : MonoBehaviour {
    public static BossHealthController Instance;

    public Animator barContainer;
    public Image bar;

    void Awake()
    {
        Instance = this;
    }

    public void Show()
    {
        barContainer.SetTrigger("Open");
    }

    public void UpdateHealth(float value)
    {
        if (value <= 0.01)
        {
            barContainer.SetTrigger("Close");
        }
        bar.fillAmount = value;
    }

}
