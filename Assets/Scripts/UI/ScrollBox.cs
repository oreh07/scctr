﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ScrollBox : MonoBehaviour {
    static Image selectedImage;

    [HideInInspector] public MarketMenuWindow mw;
    public Transform itemContainer;

    [Header("Stats")]
    public List<ItemType> items;

    public void AddItem(ItemType type)
    {
        items.Add(type);

        GameObject item = ItemController.Instance.GetUIPrefab(type);
        item.transform.SetParent(itemContainer);
        item.transform.localScale = Vector3.one;
        item.GetComponent<ItemUI>().type = type;

        item.GetComponent<EventTrigger>().triggers[0].callback.AddListener((data) =>
        {
            ItemType itemType = type;
            SelectItem(itemType);
        }
        );
    }

    public void RemoveItem(ItemType type)
    {
        foreach (Transform t in itemContainer)
        {
            if (t.GetComponent<ItemUI>().type == type)
            {
                Destroy(t.gameObject);
                return;
            }
        }
    }

    public void AddMarketButtons()
    {
        for(int i = 0; i < items.Count; i++)
        {
            ItemStat stats = ItemController.Instance.GetItemStats(items[i]);

            GameObject button = Instantiate(mw.marketButtonPrefab);
            button.transform.SetParent(itemContainer.GetChild(i));
            button.transform.localScale = Vector3.one;
            button.transform.localPosition = new Vector3(30, -80, 0);

            button.GetComponentInChildren<Text>().text = stats.price.ToString();

            button.GetComponent<EventTrigger>().triggers[0].callback.AddListener((data) =>
            {
                int price = stats.price;
                ItemType type = stats.type;
                if (PlayerController.Instance.money >= price)
                {
                    print("buy " + type.ToString());
                    TryBuy(type);
                }
            }
            );
        }
    }

    public bool TryBuy(ItemType type)
    {
        PlayerController pc = PlayerController.Instance;
        ItemController ic = ItemController.Instance;
        ItemStat stat = ItemController.Instance.GetItemStats(type);
        if (pc.money >= stat.price)
        {
            pc.money -= stat.price;
            ic.AddItem(type);

            mw.UpdatePlayerItems();
            mw.SoldItem(type);
            return true;
        }
        return false;
    }

    public void SelectItem(ItemType type)
    {
        mw.ShowDescription(type);
    }

    public void Clear()
    {
        foreach (Transform tr in itemContainer)
        {
            Destroy(tr.gameObject);
        }
        items.Clear();
    }
}
