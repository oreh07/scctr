﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public static PlayerController Instance;
    public PlayerUnit player;

    [Header("Consumables")]
    [SerializeField]
    private int _money;
    public int money
    {
        get
        {
            return _money;
        }
        set
        {
            _money = value;
            foreach (Text t in moneyValue)
            {
                t.text = _money.ToString();
            }
        }
    }
    [SerializeField]
    private int _keys;
    public int keys
    {
        get
        {
            return _keys;
        }
        set
        {
            _keys = value;
            foreach (Text t in keyValue)
            {
                t.text = _money.ToString();
            }
        }
    }
    [SerializeField]
    private Text[] moneyValue;
    [SerializeField]
    private Text[] keyValue;

    [Header("Active")]
    public Image activeSprite;
    [HideInInspector] public ActiveItem active;
    public Image activeBar;

    [Header("Teleport")]
    public float teleportCooldown = 3f;
    public float teleportDistance = 4f;
    public float teleportInvulnerable = 0.5f;
    public Image teleportBar;
    public ParticleSystem teleportParticle;
    public AnimationScript teleportSpritesIn;
    public AnimationScript teleportSpritesOut;

    private float teleportTime;
    private Vector3 mousePosition;
    private Vector3 movement;

    public bool _allowControl;
    public bool AllowControl
    {
        get
        {
            return _allowControl;
        }
        set
        {
            _allowControl = value;
            player.Rb.isKinematic = !value;
        }
    }

    public ItemType ActiveItem
    {
        get
        {
            if (active == null) return ItemType.None;
            return active.item;
        }
    }

    void Awake()
    {
        Instance = this;
        teleportTime = 0;
    }

	void Update () {
        UpdateInterface();

        if (!AllowControl)
        {
            return;
        }

        Vector3 mouse = Input.mousePosition;
        Ray screen = Camera.main.ScreenPointToRay(mouse);
        mousePosition = GetPointByPosition(screen);

        UpdateMovement();
        UpdateActive();
        UpdateShoot();
        UpdateTeleport();
        CorrectPosition();

        active.UpdateState();

        Debug.DrawLine(player.transform.position, mousePosition);
    }

    private void CorrectPosition()
    {
        Vector3 pp = player.transform.position;
        pp.x = Mathf.Clamp(pp.x, -10f, 10f);
        pp.z = Mathf.Clamp(pp.z, -13.5f, 13.5f);
        player.transform.position = pp;
    }

    public void Teleport(Vector3 direction)
    {
        Vector3 to = Vector3.zero;
        int count = 0;
        do
        {
            count++;
            if (count > 50) break;
            to = player.transform.position + direction;
            direction *= 0.97f;
        } while (Mathf.Abs(to.x) > 10.5f || Mathf.Abs(to.z) > 14f);

        StartCoroutine(LateTeleport(to));
    }

    IEnumerator LateTeleport(Vector3 to)
    {
        Vector3 pos = player.transform.position;

        AllowControl = false;
        player.SetInvisible(true);
        teleportParticle.transform.position = pos;
        teleportParticle.Play();
        teleportSpritesIn.transform.position = pos;
        teleportSpritesIn.Play();

        yield return new WaitForSeconds(teleportInvulnerable * 0.5f);

        player.transform.position = to;

        yield return new WaitForSeconds(teleportInvulnerable * 0.4f);
        teleportParticle.transform.position = to;
        teleportParticle.Play();
        teleportSpritesOut.transform.position = to;
        teleportSpritesOut.Play();

        yield return new WaitForSeconds(teleportInvulnerable * 0.1f);

        AllowControl = true;
        player.SetInvisible(false);
    }

    public void ChangeActive(ActiveItem item)
    {
        Debug.Log("Change active to " + item.item.ToString());
        if (active)
        {
            Destroy(active);
        }
        active = item;
        activeSprite.sprite = ItemController.Instance.GetItemStats(item.item).sprite;
    }
    
    void UpdateInterface()
    {
        if (active != null && active.item != ItemType.None)
        {
            activeBar.fillAmount = active.cTimer / active.cooldownTime;
        }
    }
    
    void UpdateActive()
    {
        if (active != null && active.item != ItemType.None)
        {
            if (!active.isSleep)
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    active.Activate();
                }
                else if (Input.GetKeyDown(KeyCode.Space))
                {
                    active.Deactivate();
                }
            }
        }
    }

    void UpdateShoot()
    {
        if (Input.GetMouseButton(0))
        {
            player.Shoot();
        }
    }

    void UpdateMovement()
    {
        Vector3 dm = (mousePosition - player.transform.position).normalized;
        player.RotateTo(-Mathf.Atan2(dm.z, dm.x) * 180f / Mathf.PI + 90f);

        movement.x = Input.GetAxis("Horizontal");
        movement.z = Input.GetAxis("Vertical");

        player.MoveTo(movement);
    }

    void UpdateTeleport()
    {
        if (teleportTime <= 0)
        {
            teleportBar.fillAmount = 1f;
            if (Input.GetMouseButton(1))
            {
                Vector3 dv = movement.normalized;//(mousePosition - player.transform.position).normalized;
                //float distance = Mathf.Min((mousePosition - player.transform.position).magnitude, teleportDistance);
                //dv = player.transform.position + dv * distance;
                Teleport(dv * teleportDistance);
                teleportTime = teleportCooldown;
            }
        }
        else
        {
            teleportBar.fillAmount = 1f - teleportTime / teleportCooldown;
            teleportTime -= Time.deltaTime;
        }
    }

    private Vector3 GetPointByPosition(Ray ray)
    {
        Vector3 rayHit = Vector3.zero;
        Plane p = new Plane(Vector3.up, Vector3.zero);
        float hitDist;
        if (p.Raycast(ray, out hitDist))
        {
            return ray.GetPoint(hitDist);
        }
        return Vector3.zero;
    }

    public void ClearPlayer()
    {
        money = 0;
        keys = 1;
        PreparePlayerPosition();
        player.RestoreDefaultStats();
    }

    public void PreparePlayerPosition()
    {
        player.transform.position = new Vector3(0, 0, -17f);
        player.transform.rotation = Quaternion.identity;
    }
}
