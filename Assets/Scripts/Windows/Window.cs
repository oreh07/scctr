﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Window : MonoBehaviour {
    protected MenuController mc;

    void Start()
    {
        mc = MenuController.Instance;
        Init();
    }
    void Update() { }

    public virtual void Init() { }

    public virtual void Activate()
    {
        gameObject.SetActive(true);
    }
    public virtual void Deactivate()
    {
        gameObject.SetActive(false);
    }
}
