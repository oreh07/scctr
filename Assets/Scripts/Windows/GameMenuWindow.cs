﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;

public class GameMenuWindow : Window {
    private bool isPaused = false;

    public GameObject gameWindow;
    public Text levelName;
    public Text levelDescription;

    [Header("Pause")]
    public GameObject pauseWindow;
    public Text itemDescription;
    public Transform pauseItemContainer;

    public override void Activate()
    {
        base.Activate();
        LevelController.Instance.currentLevel.Activate();

        StartCoroutine(LateStartLevelAnimation());

        PauseGame(false);
        UpdateItems();
        UnityEngine.Cursor.visible = false;
    }

    public override void Deactivate()
    {
        base.Deactivate();
        UnityEngine.Cursor.lockState = CursorLockMode.Confined;
        UnityEngine.Cursor.visible = true;
    }

    public void PauseGame(bool value)
    {
        isPaused = value;
        UpdateItems();
        if (value)
        {
            Time.timeScale = 0f;
            gameWindow.SetActive(false);
            pauseWindow.SetActive(true);
            if (ItemController.Instance.itemsPlayerhave.Count > 0)
            {
                SelectItem(ItemController.Instance.itemsPlayerhave[0]);
            }
        }
        else
        {
            Time.timeScale = 1f;
            gameWindow.SetActive(true);
            pauseWindow.SetActive(false);
            ItemUI.selected = ItemType.None;
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PauseGame(!isPaused);
        }
    }

    IEnumerator LateStartLevelAnimation()
    {
        yield return new WaitForSeconds(1.0f);

        //PlayerController.Instance.player.transform.DOMove(new Vector3(0, 0, -8f), 1f);

        yield return new WaitForSeconds(0.7f);

    }

    void UpdateItems()
    {
        foreach (Transform t in pauseItemContainer)
        {
            Destroy(t.gameObject);
        }
        ItemController it = ItemController.Instance;
        List<ItemType> items = it.itemsPlayerhave;
        for(int i = 0; i < items.Count; i++)
        {
            GameObject button = it.GetUIPrefab(items[i]);
            button.transform.SetParent(pauseItemContainer);
            button.transform.localScale = Vector3.one;
            button.GetComponent<ItemUI>().type = items[i];
            
            ItemType t = items[i];
            button.GetComponent<EventTrigger>().triggers[0].callback.AddListener((data) =>
            {
                SelectItem(t);
            }
            );
        }
    }

    void SelectItem(ItemType item)
    {
        print("select " + item.ToString());

        ItemStat stat = ItemController.Instance.GetItemStats(item);
        itemDescription.text = stat.comment + "\n" + stat.description;

        ItemUI.selected = item;
    }
}
