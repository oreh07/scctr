﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour {
    private static MenuController _instance;
    public static MenuController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<MenuController>();
            }
            return _instance;
        }
    }
    
    public static event OnLevelChanged onLevelChanged;
    public delegate void OnLevelChanged(Level levelContainer);

    public StartMenuWindow startMenuWindow;
    public GameMenuWindow gameMenuWindow;
    public MarketMenuWindow marketMenuWindow;

    [Header("Started Items")]
    [SerializeField]
    ItemType[] startedItems;

    void Start()
    {
        gameMenuWindow.Deactivate();
        marketMenuWindow.Deactivate();
        startMenuWindow.Activate();

        StartCoroutine(SetupStartedItems());
        //ItemController.Instance.AddItem(ItemType.BankaWithRainbow);
        //ItemController.Instance.AddItem(ItemType.SmartPodarok);
    }

    IEnumerator SetupStartedItems()
    {
        foreach (ItemType item in startedItems)
        {
            ItemController.Instance.AddItem(item);

            yield return new WaitForEndOfFrame();
        }
    }

    public void RestartGame()
    {
        print("restartGame");
        Time.timeScale = 1f;
        Lose();
    }

    public void LevelEnd()
    {
        print("levelEnd");
        gameMenuWindow.Deactivate();
        marketMenuWindow.Activate();
    }

    public void CloseMarket()
    {
        print("closeMarket");

        marketMenuWindow.PrepareDeactivate();
        PrepareNextLevel();
        gameMenuWindow.Activate();
    }

    public void StartNewGame()
    {
        print("StartNewGame");
        gameMenuWindow.Activate();
        PlayerController.Instance.ClearPlayer();
    }

    public void PrepareNextLevel()
    {
        LevelController.Instance.LoadLevel(LevelType.EpicenterOfTheCity);
        onLevelChanged.Invoke(LevelController.Instance.currentLevel);
        PlayerController.Instance.PreparePlayerPosition();
        HealthController.Instance.UpdateHealth();
    }

    public void OpenMarket()
    {
        print("OpenMarket");
        gameMenuWindow.Deactivate();
        marketMenuWindow.Activate();
    }

    public void Lose()
    {
        print("Lose");
        startMenuWindow.Activate();
        startMenuWindow.anim.SetTrigger("SetFail");
        gameMenuWindow.Deactivate();
    }

    public void ClearWorld()
    {
        print("Clear world");
        LevelController.Instance.DestroyWorld();
        BulletController.Instance.ClearBullets();
        BotController.Instance.DestroyAll();
        
    }
}
