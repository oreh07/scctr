﻿using System.Collections.Generic;
using UnityEngine;

public class StartMenuWindow : Window {

    public Animator anim;

    public List<Transform> texts;
    public List<Transform> words;
    private float pressTimer = 0f;

    private bool startingGame = false;
    
	void Update () {
        if (startingGame) return;

        int shake = 0;

        if (Input.GetKey(KeyCode.A))
        {
            shake += 10;
        }
        if (Input.GetKey(KeyCode.R))
        {
            shake += 10;
        }
        if (Input.GetKey(KeyCode.G))
        {
            shake += 10;
        }
        if (Input.GetKey(KeyCode.H))
        {
            shake += 10;
        }

        if (shake >= 3)
        {
            pressTimer += Time.deltaTime;
            if (pressTimer > 1f)
            {
                pressTimer = 0;
                StartGame();
            }
        }
        else
        {
            pressTimer = 0;
        }
        //print(Input.GetKey(KeyCode.A) + " " + Input.GetKey(KeyCode.R) + " " + Input.GetKey(KeyCode.G) + " " + Input.GetKey(KeyCode.H));

        foreach (Transform t in texts)
        {
            t.GetChild(0).localPosition = Vector3Extension.GetRandom(shake * 0.5f);
        }
        foreach (Transform t in words)
        {
            t.GetChild(0).localPosition = Vector3Extension.GetRandom(shake);
        }
	}

    public void NewGame()
    {
        anim.SetTrigger("SetNew");
    }

    private void StartGame()
    {
        anim.SetTrigger("SetStart");
        mc.PrepareNextLevel();
        startingGame = true;
    }

    public override void Activate()
    {
        base.Activate();
        startingGame = false;
    }

    public override void Deactivate()
    {
        base.Deactivate();
        if (startingGame)
        {
            startingGame = false;
            mc.StartNewGame();
        }
    }

    private void ClearWorld()
    {
        mc.ClearWorld();
    }
}
