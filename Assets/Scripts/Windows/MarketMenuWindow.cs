﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MarketMenuWindow : Window {

    public Text description;

    public ScrollBox marketList;
    public ScrollBox playerList;
    public Animator anim;

    public GameObject marketButtonPrefab;
    
    public void OnOpen()
    {
        mc.ClearWorld();
    }

    public override void Activate()
    {
        base.Activate();
        marketList.mw = this;
        playerList.mw = this;

        anim.SetTrigger("setOpen");

        FillMarket();
        UpdatePlayerItems();
    }

    public override void Deactivate()
    {
        base.Deactivate();
        Clear();
    }

    public void PrepareDeactivate()
    {
        anim.SetTrigger("setClose");
    }

    private void FillMarket()
    {
        ItemController ic = ItemController.Instance;

        // items (acitve and passive)
        List<ItemType> marketItemPool = ic.CollectList(ItemClass.Active | ItemClass.Passive);
        marketItemPool.RemoveAll(item => ic.itemsPlayerhave.Contains(item));
        AddSeveralItemsFrom(marketItemPool, 2, 3, true);
        
        marketItemPool = ic.CollectList(ItemClass.Consumable);
        AddSeveralItemsFrom(marketItemPool, 3, 3);

        marketList.AddMarketButtons();
    }

    private void AddSeveralItemsFrom(List<ItemType> items, int min, int max, bool removeItemAfter = false)
    {
        ItemController ic = ItemController.Instance;

        int count = Mathf.Clamp(items.Count, min, max);
        int bug = 0;
        ItemType it;
        for (int i = 0; i < count; i++)
        {
            bug = 0;
            do
            {
                bug++;
                if (bug > 100)
                {
                    Debug.LogError("[MarketMenuWindow] cant find item");
                    Debug.Break();
                    it = ItemType.Key;
                    break;
                }
                it = items[Random.Range(0, items.Count)];
            } while (ic.itemsPlayerhave.Contains(it));

            if (removeItemAfter)
            {
                items.Remove(it);
            }

            marketList.AddItem(it);
        }
    }

    public void UpdatePlayerItems()
    {
        playerList.Clear();
        foreach (ItemType i in ItemController.Instance.itemsPlayerhave)
        {
            playerList.AddItem(i);
        }
    }

    public void ShowDescription(ItemType item)
    {
        ItemStat stat = ItemController.Instance.GetItemStats(item);
        description.text = stat.comment + "\n" + stat.description;
    }

    public void SoldItem(ItemType type)
    {
        marketList.RemoveItem(type);
    }

    public void Clear()
    {
        marketList.Clear();
        playerList.Clear();
    }
}
